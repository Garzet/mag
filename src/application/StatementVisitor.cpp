#include "application/StatementVisitor.hpp"

#include "application/ExpressionVisitor.hpp"
#include "Visitor.hpp"
#include "Transformations.hpp"

#include <sstream>
#include <cassert>
#include <algorithm>

namespace MAG::Application {

namespace {

[[nodiscard]] MAO::HostRefVector get_lvalue_hosts(Out::LValueMap lvalues) noexcept;

[[nodiscard]] MAO::HostRefVector get_affected_hosts(const Out::ComponentAbstractSyntaxTreeMap& then_asts,
                                                    const Out::ComponentAbstractSyntaxTreeMap& else_asts) noexcept;

[[nodiscard]] MAO::HostRefVector get_affected_hosts(const Out::ComponentAbstractSyntaxTreeMap& conditon_asts,
                                                    const Out::PartialExpressionMap& condition_expressions) noexcept;

[[nodiscard]] MAO::HostRefVector
get_for_statement_condition_step_body_affected_hosts(const MAL::AST::ForStatement& for_statement,
                                                     const Out::ComponentAbstractSyntaxTreeMap& body_asts,
                                                     const Out::ComponentAbstractSyntaxTreeMap& step_asts,
                                                     const MAO::Mapping& mapping) noexcept;

}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::Assignment& assignment) const noexcept
{
    auto lresult = boost::apply_visitor(LValueVisitor{mapping}, assignment.left_side);
    auto rresult = boost::apply_visitor(AssignmentRightSideVisitor{get_lvalue_hosts(lresult.lvalues), mapping},
                                        assignment.right_side);

    Out::ComponentAbstractSyntaxTreeMap result{std::move(rresult.asts) + std::move(lresult.asts)};
    for (auto& [host, lvalue] : lresult.lvalues) {
        const auto right_side_it = rresult.partial_expressions.find(host);
        assert(right_side_it != rresult.partial_expressions.cend());
        result[host].add(
            Out::Assignment{std::move(lvalue), Out::AssignmentOperator::Equals, std::move(right_side_it->second)});
    }

    return result;
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::Send& send) const noexcept
{
    Out::ComponentAbstractSyntaxTreeMap result;
    for (const auto& var_ref : send) {
        assert(var_ref.variable_identifier.declaration);
        const auto& write_host = MAO::get_hosts(mapping, *var_ref.variable_identifier.declaration).front().get();

        std::optional<Out::Expression> index_expression;
        if (var_ref.index) {
            auto index_result = ArithmeticExpressionVisitor{{write_host}, mapping}(*var_ref.index, true);
            result            = std::move(result) + std::move(index_result.asts);
            const auto it     = index_result.partial_expressions.find(write_host);
            assert(it != index_result.partial_expressions.cend());
            index_expression = std::move(it->second);
        }

        result[write_host].add(Out::VoidFunctionCall{
            "write",
            {Out::VariableReference{std::nullopt, var_ref.variable_identifier, std::move(index_expression)},
             Out::InitializerList{{Out::StringLiteral{"Client"}}}},
        });
    }
    return result;
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::Receive& receive) const noexcept
{
    Out::ComponentAbstractSyntaxTreeMap asts;
    for (const auto& var_decl : receive) {
        const auto& hosts   = MAO::get_hosts(mapping, var_decl);
        const auto type_str = to_out(var_decl.type);
        const Out::Assignment assignment{Out::VariableDeclaration{"auto", var_decl.variable_name},
                                         Out::AssignmentOperator::Equals,
                                         Out::FunctionCall{"read<" + type_str + '>', {Out::StringLiteral{"Client"}}}};
        for (const MAO::Host& host : hosts) { asts[host].add(assignment); }
    }
    return asts;
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::Print& print) const
{
    throw PrintStatementEncounteredError{print};
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::IfStatement& ifstmt) const noexcept
{
    auto then_asts = apply_to_all(ifstmt.then_statements);
    auto else_asts = apply_to_all(ifstmt.else_statements);

    auto [result, condition_expressions] =
        ArithmeticExpressionVisitor{get_affected_hosts(then_asts, else_asts), mapping}(ifstmt.condition);

    for (auto& [host, condition_expression] : condition_expressions) {
        result[host].add(Out::IfStatement{std::move(condition_expression),
                                          std::move(then_asts[host].get_statements()),
                                          std::move(else_asts[host].get_statements())});
    }

    return std::move(result);
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::operator()(const MAL::AST::ForStatement& forstmt) const noexcept
{
    Out::ComponentAbstractSyntaxTreeMap result;
    if (forstmt.init_statement) { result = operator()(*forstmt.init_statement); };

    auto body_asts = apply_to_all(forstmt.body);

    Out::ComponentAbstractSyntaxTreeMap step_asts;
    if (forstmt.step_statement) { step_asts = operator()(*forstmt.step_statement); };

    auto [condition_asts, condition_expressions] = ArithmeticExpressionVisitor{
        get_for_statement_condition_step_body_affected_hosts(forstmt, body_asts, step_asts, mapping),
        mapping}(forstmt.condition);

    assert(std::all_of(
        condition_asts.cbegin(),
        condition_asts.cend(),
        [&condition_expressions = condition_expressions](
            const std::pair<std::reference_wrapper<const MAO::Host>, Out::ComponentAbstractSyntaxTree>& pair) noexcept
        -> bool { return condition_expressions.find(pair.first) != condition_expressions.cend(); }));

    for (auto& [host, expression] : condition_expressions) {
        auto lambda_statements = std::move(condition_asts[host].get_statements());
        if (!lambda_statements.empty()) {
            lambda_statements.push_back(Out::Return{std::move(expression)});
            expression = Out::LambdaCall{Out::Lambda{{Out::Literal{"&"}}, {}, std::move(lambda_statements)}, {}};
        }
    }

    for (auto& [host, condition_expression] : condition_expressions) {
        auto body_ast = std::move(body_asts[host]) + std::move(step_asts[host]);
        result[host].add(Out::WhileStatement{std::move(condition_expression), std::move(body_ast.get_statements())});
    }

    return result;
}

Out::ComponentAbstractSyntaxTreeMap StatementVisitor::apply_to_all(const std::vector<MAL::AST::Statement>& ast) const
{
    Out::ComponentAbstractSyntaxTreeMap result;
    for (const auto& statement : ast) { result = std::move(result) + boost::apply_visitor(*this, statement); }
    return result;
}

Out::ComponentAbstractSyntaxTreeMap generate(const MAO::Mapping& mapping, const std::vector<MAL::AST::Statement>& ast)
{
    return StatementVisitor{mapping}.apply_to_all(ast);
}

PrintStatementEncounteredError::PrintStatementEncounteredError(const MAL::AST::Print& print) noexcept
        : std::runtime_error{format_what_message(print)}
{}

std::string PrintStatementEncounteredError::format_what_message(const MAL::AST::Print& print_statement) const noexcept
{
    std::ostringstream oss;
    oss << "Application side print statement encountered, but not suppored.\nPrint statement:\n";
    MAL::AST::PrintVisitor{oss}(print_statement);
    return oss.str();
}

namespace {

MAO::HostRefVector get_lvalue_hosts(Out::LValueMap lvalues) noexcept
{
    MAO::HostRefVector result;
    result.reserve(lvalues.size());
    for (const auto& [host, lvalue] : lvalues) { result.push_back(host); }
    return result;
}

MAO::HostRefVector get_affected_hosts(const Out::ComponentAbstractSyntaxTreeMap& then_asts,
                                      const Out::ComponentAbstractSyntaxTreeMap& else_asts) noexcept
{
    MAO::HostRefVector result;
    result.reserve(then_asts.size() + else_asts.size());
    for (const auto& [host, ast] : then_asts) { result.push_back(host); }
    for (const auto& [host, ast] : else_asts) { result.push_back(host); }
    return result;
}

MAO::HostRefVector get_affected_hosts(const Out::ComponentAbstractSyntaxTreeMap& conditon_asts,
                                      const Out::PartialExpressionMap& condition_expressions) noexcept
{
    MAO::HostRefVector result;
    result.reserve(conditon_asts.size() + condition_expressions.size());
    for (const auto& [host, ast] : conditon_asts) { result.push_back(host); }
    for (const auto& [host, ast] : condition_expressions) { result.push_back(host); }
    return result;
}

MAO::HostRefVector
get_for_statement_condition_step_body_affected_hosts(const MAL::AST::ForStatement& for_statement,
                                                     const Out::ComponentAbstractSyntaxTreeMap& body_asts,
                                                     const Out::ComponentAbstractSyntaxTreeMap& step_asts,
                                                     const MAO::Mapping& mapping) noexcept
{
    // NOTE: This function is fairly inefficient since in order to determine affected hosts, ASTs and partial
    //       expressions are generated. Better approach would be to implement expression visitor that only computes
    //       affected hosts and does not produce ASTs and partial expressions.

    auto result = get_affected_hosts(body_asts, step_asts);

    const auto [uncomplete_condition_asts, uncomplete_condtion_expressions] =
        ArithmeticExpressionVisitor{result, mapping}(for_statement.condition);

    const auto condition_affected_hosts =
        get_affected_hosts(uncomplete_condition_asts, uncomplete_condtion_expressions);
    for (const auto condition_affected_host : condition_affected_hosts) { result.push_back(condition_affected_host); }

    return result;
}

}

}
