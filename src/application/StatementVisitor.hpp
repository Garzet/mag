#ifndef MAG_APPLICATION_STATEMENTVISITOR_HPP
#define MAG_APPLICATION_STATEMENTVISITOR_HPP

#include "out/AbstractSyntaxTree.hpp"
#include "out/ComponentAbstractSyntaxTree.hpp"

#include <MAO/Mapping.hpp>
#include <MAL.hpp>

#include <string>
#include <stdexcept>
#include <type_traits>

namespace MAG::Application {

class StatementVisitor final {
  public:
    std::reference_wrapper<const MAO::Mapping> mapping;

  public:
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::Assignment& assignment) const noexcept;
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::Send& send) const noexcept;
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::Receive& receive) const noexcept;
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::Print& print) const;
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::IfStatement& ifstmt) const noexcept;
    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap operator()(const MAL::AST::ForStatement& forstmt) const noexcept;

    [[nodiscard]] Out::ComponentAbstractSyntaxTreeMap apply_to_all(const std::vector<MAL::AST::Statement>& ast) const;
};

[[nodiscard]] Out::ComponentAbstractSyntaxTreeMap generate(const MAO::Mapping& mapping,
                                                           const std::vector<MAL::AST::Statement>& ast);

class PrintStatementEncounteredError final : public std::runtime_error {
  public:
    explicit PrintStatementEncounteredError(const MAL::AST::Print& print_statement) noexcept;

  private:
    [[nodiscard]] std::string format_what_message(const MAL::AST::Print& print_statement) const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<PrintStatementEncounteredError>);

}

#endif
