#include "client/ExpressionVisitor.hpp"

#include "Transformations.hpp"

#include <sstream>
#include <iomanip>
#include <cassert>

namespace MAG::Client {

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::PrefixExpression& pref_expr) const
{
    auto out_expr = boost::apply_visitor(PrimaryExpressionVisitor{}, pref_expr.primary_expression);
    if (pref_expr.prefix_operator) {
        out_expr = Out::PrefixOperation{to_out(*pref_expr.prefix_operator), std::move(out_expr)};
    }
    return out_expr;
}

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::MultiplicationExpression& mul_expr) const
{
    auto first_out_expr = operator()(mul_expr.first_expression);
    if (mul_expr.other_expressions.empty()) { return first_out_expr; }

    const auto& oe               = mul_expr.other_expressions;
    const auto first_mul_op      = oe.front().multiplication_operator;
    const auto& second_pref_expr = oe.front().prefix_expression;
    auto second_out_expr         = operator()(second_pref_expr);

    Out::BinaryOperation out_bin_op{to_out(first_mul_op), std::move(first_out_expr), std::move(second_out_expr)};

    for (auto it = oe.begin() + 1; it != oe.end(); it++) {
        auto out_expr = operator()(it->prefix_expression);
        out_bin_op    = Out::BinaryOperation{to_out(it->multiplication_operator), out_bin_op, std::move(out_expr)};
    }

    return out_bin_op;
}

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::AdditionExpression& add_expr) const
{
    auto first_out_expr = operator()(add_expr.first_expression);
    if (add_expr.other_expressions.empty()) { return first_out_expr; }

    const auto& oe              = add_expr.other_expressions;
    const auto first_add_op     = oe.front().addition_operator;
    const auto& second_mul_expr = oe.front().multiplication_expression;
    auto second_out_expr        = operator()(second_mul_expr);

    Out::BinaryOperation out_bin_op{to_out(first_add_op), std::move(first_out_expr), std::move(second_out_expr)};

    for (auto it = oe.begin() + 1; it != oe.end(); it++) {
        auto out_expr = operator()(it->multiplication_expression);
        out_bin_op    = Out::BinaryOperation{to_out(it->addition_operator), out_bin_op, std::move(out_expr)};
    }

    return out_bin_op;
}

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::ComparisonExpression& comp_expr) const
{
    auto first_out_expr = operator()(comp_expr.first_expression);
    if (comp_expr.other_expressions.empty()) { return first_out_expr; }

    const auto& oe              = comp_expr.other_expressions;
    const auto first_comp_op    = oe.front().comparison_operator;
    const auto& second_add_expr = oe.front().addition_expression;
    auto second_out_expr        = operator()(second_add_expr);

    Out::BinaryOperation out_bin_op{to_out(first_comp_op), std::move(first_out_expr), std::move(second_out_expr)};

    for (auto it = oe.begin() + 1; it != oe.end(); it++) {
        auto out_expr = operator()(it->addition_expression);
        out_bin_op    = Out::BinaryOperation{to_out(it->comparison_operator), out_bin_op, std::move(out_expr)};
    }

    return out_bin_op;
}

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::BooleanAndExpression& and_expr) const
{
    auto first_out_expr = operator()(and_expr.first_expression);
    if (and_expr.other_expressions.empty()) { return first_out_expr; }

    const auto& oe = and_expr.other_expressions;
    assert(oe.front().logical_operator == MAL::AST::LogicalOperator::And);
    const auto& second_comp_expr = oe.front().comparison_expression;
    auto second_out_expr         = operator()(second_comp_expr);

    Out::BinaryOperation out_bin_op{
        MAG::Out::BinaryOperator::LogicalAnd, std::move(first_out_expr), std::move(second_out_expr)};

    for (auto it = oe.begin() + 1; it != oe.end(); it++) {
        assert(it->logical_operator == MAL::AST::LogicalOperator::And);
        auto out_expr = operator()(it->comparison_expression);
        out_bin_op    = Out::BinaryOperation{MAG::Out::BinaryOperator::LogicalAnd, out_bin_op, std::move(out_expr)};
    }

    return Out::Expression{std::move(out_bin_op)};
}

Out::Expression ArithmeticExpressionVisitor::operator()(const MAL::AST::BooleanOrExpression& or_expr) const
{
    auto first_out_expr = operator()(or_expr.first_expression);
    if (or_expr.other_expressions.empty()) { return first_out_expr; }

    const auto& oe = or_expr.other_expressions;
    assert(oe.front().logical_operator == MAL::AST::LogicalOperator::Or);
    const auto& second_and_expr = oe.front().boolean_and_expression;
    auto second_out_expr        = operator()(second_and_expr);

    Out::BinaryOperation out_bin_op{
        MAG::Out::BinaryOperator::LogicalOr, std::move(first_out_expr), std::move(second_out_expr)};

    for (auto it = oe.begin() + 1; it != oe.end(); it++) {
        assert(it->logical_operator == MAL::AST::LogicalOperator::Or);
        auto out_expr = operator()(it->boolean_and_expression);
        out_bin_op    = Out::BinaryOperation{MAG::Out::BinaryOperator::LogicalOr, out_bin_op, std::move(out_expr)};
    }

    return out_bin_op;
}

Out::Expression PrimaryExpressionVisitor::operator()(const MAL::AST::VariableReference& /*v_ref*/) const
{
    assert(false);
    throw std::logic_error{"Not yet implemented!"};
}

Out::Expression PrimaryExpressionVisitor::operator()(const MAL::AST::NumericLiteral& num_lit) const
{
    auto num_lit_as_string = boost::apply_visitor(
        [](auto&& numeric_literal) -> std::string {
            std::ostringstream oss;
            oss << std::boolalpha << numeric_literal;
            return oss.str();
        },
        num_lit);
    return Out::Literal{std::move(num_lit_as_string)};
}

Out::Expression PrimaryExpressionVisitor::operator()(const MAL::AST::ArrayLiteral& arr_lit) const
{
    assert(false);
    throw std::logic_error{"Not yet implemented!"};
}

Out::Expression PrimaryExpressionVisitor::operator()(const MAL::AST::LenFunctionCall& len_fn_call) const
{
    assert(false);
    throw std::logic_error{"Not yet implemented!"};
}

Out::VariableDeclaration LValueVisitor::operator()(const MAL::AST::VariableDeclaration& vd) const
{
    std::string out_type = to_out(vd.type);
    if (vd.is_array_declaration) { out_type = "std::vector<" + out_type + '>'; }
    return Out::VariableDeclaration{std::move(out_type), vd.variable_name};
}

Out::VariableDeclaration LValueVisitor::operator()(const MAL::AST::VariableReference& vr) const
{
    assert(false);
    throw std::logic_error{"Not yet implemented!"};
}

}
