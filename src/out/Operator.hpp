#ifndef MAG_OUT_OPERATOR_HPP
#define MAG_OUT_OPERATOR_HPP

namespace MAG::Out {

enum class PrefixOperator : unsigned char {
    Not /* ! */
};

enum class BinaryOperator : unsigned char {
    Plus,          /*  +   */
    Minus,         /*  -   */
    Multiply,      /*  *   */
    Divide,        /*  /   */
    Equals,        /*  ==  */
    NotEquals,     /*  !=  */
    Less,          /*  <   */
    LessEquals,    /*  <=  */
    Greater,       /*  >   */
    GreaterEquals, /*  >=  */
    LeftShift,     /*  <<  */
    RightShift,    /*  >>  */
    LogicalAnd,    /*  &&  */
    LogicalOr      /*  ||  */
};

enum class AssignmentOperator : unsigned char {
    Equals,         /*   =  */
    PlusEquals,     /*  +=  */
    MinusEquals,    /*  -=  */
    MultiplyEquals, /*  *=  */
    DivideEquals    /*  /=  */
};

}

#endif
