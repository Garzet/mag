#ifndef MAG_OUT_QUERYVISITOR_HPP
#define MAG_OUT_QUERYVISITOR_HPP

#include "AbstractSyntaxTree.hpp"

#include <type_traits>

namespace MAG::Out {

template<typename T, typename... U>
constexpr bool is_any_of_v = (std::is_same_v<T, U> || ...);

template<typename DesiredNodeType>
class QueryVisitor final {
  public:
    using DesiredNodeTypeNoConst = std::remove_const_t<DesiredNodeType>;
    static_assert(is_any_of_v<DesiredNodeTypeNoConst,
                              Assignment,
                              VoidFunctionCall,
                              FunctionDefinition,
                              ClassDefinition,
                              StructDefinition,
                              ConstructorDefinition,
                              ConstructorCall,
                              Visibility,
                              PreProcessor,
                              Return,
                              ExpressionStatement,
                              IfStatement,
                              VariableDeclaration,
                              PrefixOperation,
                              BinaryOperation,
                              Lambda,
                              LambdaCaptureDeclaration,
                              FunctionCall,
                              StructuredBiding,
                              VariableReference,
                              Literal,
                              StringLiteral,
                              MethodCall,
                              InitializerList>,
                  "Invalid DesiredNodeType for the QueryVisitor.");

    /* Invoked when the current node is of the desired type
     * (e.g. ClassDefinition) in order to check weather or not the current node
     * fulfills some arbitrary requirements (e.g. name of the class is C0). */
    using Predicate = std::function<bool(const DesiredNodeType& current_node)>;

  private:
    // If the desired node type is const, return the provided type as const.
    // This enables a single query visitor over the const and non-const AST.
    template<typename T>
    using C = std::conditional_t<std::is_const_v<DesiredNodeType>, const T, T>;

    /* Invoked when the current node is not of the desired type or the current
     * node does not satisfy the predicate function in order to enable deeper
     * search of the AST. (e.g. desired node is Identifier, current node is
     * Assignment - the function should visit the left and the right side of the
     * Assignemnt to search for the Identifier node). */
    template<typename CurrentNodeType>
    using AdvanceAction = std::function<DesiredNodeType*(C<CurrentNodeType>& current_node)>;

  private:
    Predicate predicate;

  public:
    explicit QueryVisitor(Predicate predicate = [](const DesiredNodeType& /*node*/) noexcept -> bool {
        return true;
    }) noexcept;

    [[nodiscard]] DesiredNodeType* apply_to_all(C<std::vector<Statement>>& ast) const noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<Assignment>& assignment) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<VoidFunctionCall>& void_function_call) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<FunctionDefinition>& func_definition) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ClassDefinition>& class_definition) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<StructDefinition>& struct_definition) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ConstructorDefinition>& constructor) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ConstructorCall>& constructor_call) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Visibility>& visibility) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<PreProcessor>& pre_processor) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Return>& return_statement) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ExpressionStatement>& expr_statement) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<IfStatement>& if_statement) const noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<VariableDeclaration>& declaration) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<PrefixOperation>& prefix_operation) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<BinaryOperation>& binary_operation) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Lambda>& lambda) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<LambdaCaptureDeclaration>& declaration) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<FunctionCall>& function_call) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<StructuredBiding>& binding) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<VariableReference>& variable_reference) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Literal>& literal) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<StringLiteral>& literal) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<MethodCall>& method_call) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<InitializerList>& init_list) const noexcept;

  private:
    /* Returns a pointer to the current node if it is of the desired type and it
     * satisfies the predicate function. Otherwise, the provided non-matching
     * action is invoked with the current node which allows further search or a
     * simple nullptr return if the current node is a leaf of the AST. */
    template<typename CurrentNodeType>
    [[nodiscard]] DesiredNodeType* match(C<CurrentNodeType>& current_node,
                                         AdvanceAction<CurrentNodeType> advance_action) const noexcept;

    /* Applies this visitor to the provided vector of expressions and returns a
     * pointer to the first expression of the desired type that satisfies the
     * predicate function. If no such node is found, nullptr is returned. */
    [[nodiscard]] DesiredNodeType* visit_range(C<std::vector<Expression>>& expressions) const noexcept;
};

template<typename DesiredNodeType>
QueryVisitor<DesiredNodeType>::QueryVisitor(Predicate predicate) noexcept : predicate{std::move(predicate)}
{}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::apply_to_all(C<std::vector<Statement>>& ast) const noexcept
{
    for (auto&& statement : ast) {
        if (auto* res = boost::apply_visitor(*this, statement)) { return res; }
    }
    return nullptr;
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Assignment>& assignment) const noexcept
{
    return match<Assignment>(assignment, [this](C<Assignment>& assignment) noexcept -> DesiredNodeType* {
        if (auto* res = boost::apply_visitor(*this, assignment.lvalue)) { return res; }
        return boost::apply_visitor(*this, assignment.rvalue);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VoidFunctionCall>& void_function_call) const noexcept
{
    return match<VoidFunctionCall>(void_function_call,
                                   [this](C<VoidFunctionCall>& void_function_call) noexcept -> DesiredNodeType* {
                                       return this->visit_range(void_function_call.arguments);
                                   });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<FunctionDefinition>& func_definition) const noexcept
{
    return match<FunctionDefinition>(func_definition,
                                     [this](C<FunctionDefinition>& func_definition) noexcept -> DesiredNodeType* {
                                         return this->apply_to_all()(func_definition.body);
                                     });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ClassDefinition>& class_definition) const noexcept
{
    return match<ClassDefinition>(class_definition,
                                  [this](C<ClassDefinition>& class_definition) noexcept -> DesiredNodeType* {
                                      return this->apply_to_all(class_definition.body);
                                  });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<StructDefinition>& struct_definition) const noexcept
{
    return match<StructDefinition>(struct_definition,
                                   [this](C<StructDefinition>& struct_definition) noexcept -> DesiredNodeType* {
                                       return this->apply_to_all(struct_definition.body);
                                   });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ConstructorDefinition>& constructor) const noexcept
{
    return match<ConstructorDefinition>(constructor,
                                        [this](C<ConstructorDefinition>& constructor) noexcept -> DesiredNodeType* {
                                            for (auto&& [name, arguments] : constructor.member_initializers) {
                                                if (auto* res = visit_range(arguments)) { return res; }
                                            }
                                            return this->apply_to_all(constructor.body);
                                        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ConstructorCall>& constructor_call) const noexcept
{
    return match<ConstructorCall>(constructor_call,
                                  [this](C<ConstructorCall>& constructor_call) noexcept -> DesiredNodeType* {
                                      return visit_range(constructor_call.arguments);
                                  });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Visibility>& visibility) const noexcept
{
    return match<Visibility>(visibility,
                             [](C<Visibility>& /*visibility*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<PreProcessor>& pre_processor) const noexcept
{
    return match<PreProcessor>(pre_processor,
                               [](C<PreProcessor>& /*pre_processor*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Return>& return_statement) const noexcept
{
    return match<Return>(return_statement, [this](C<Return>& return_statement) noexcept -> DesiredNodeType* {
        return boost::apply_visitor(*this, return_statement.expression);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ExpressionStatement>& expr_statement) const noexcept
{
    return match<ExpressionStatement>(expr_statement,
                                      [this](C<ExpressionStatement>& expr_statement) noexcept -> DesiredNodeType* {
                                          return boost::apply_visitor(*this, expr_statement.expression);
                                      });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<IfStatement>& if_statement) const noexcept
{
    return match<IfStatement>(if_statement, [this](C<IfStatement>& if_statement) noexcept -> DesiredNodeType* {
        if (auto* p = boost::apply_visitor(*this, if_statement.condition)) { return p; }
        if (auto* p = this->apply_to_all(if_statement.then_statements)) { return p; }
        return this->apply_to_all(if_statement.else_statements);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VariableDeclaration>& declaration) const noexcept
{
    return match<VariableDeclaration>(
        declaration, [](C<VariableDeclaration>& /*declaration*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<PrefixOperation>& prefix_operation) const noexcept
{
    return match<PrefixOperation>(prefix_operation,
                                  [this](C<PrefixOperation>& prefix_operation) noexcept -> DesiredNodeType* {
                                      return boost::apply_visitor(*this, prefix_operation.expression);
                                  });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<BinaryOperation>& binary_operation) const noexcept
{
    return match<BinaryOperation>(binary_operation,
                                  [this](C<BinaryOperation>& binary_operation) noexcept -> DesiredNodeType* {
                                      if (auto* res = boost::apply_visitor(*this, binary_operation.left_side)) {
                                          return res;
                                      }
                                      return boost::apply_visitor(*this, binary_operation.right_side);
                                  });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Lambda>& lambda) const noexcept
{
    return match<Lambda>(lambda, [this](C<Lambda>& lambda) noexcept -> DesiredNodeType* {
        if (auto* res = visit_range(lambda.capture)) { return res; }
        return this->apply_to_all(lambda.body);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<LambdaCaptureDeclaration>& declaration) const noexcept
{
    return match<LambdaCaptureDeclaration>(
        declaration, [this](C<LambdaCaptureDeclaration>& declaration) noexcept -> DesiredNodeType* {
            return boost::apply_visitor(*this, declaration.expression);
        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<FunctionCall>& function_call) const noexcept
{
    return match<FunctionCall>(function_call, [this](C<FunctionCall>& function_call) noexcept -> DesiredNodeType* {
        return visit_range(function_call.arguments);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<StructuredBiding>& binding) const noexcept
{
    return match<StructuredBiding>(
        binding, [this](C<StructuredBiding>& /*binding*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VariableReference>& variable_reference) const noexcept
{
    return match<VariableReference>(
        variable_reference,
        [](C<VariableReference>& /*variable_reference*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Literal>& literal) const noexcept
{
    return match<Literal>(literal, [](C<Literal>& /*literal*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<StringLiteral>& string_literal) const noexcept
{
    return match<StringLiteral>(
        string_literal, [](C<StringLiteral>& /*string_literal*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<MethodCall>& method_call) const noexcept
{
    return match<MethodCall>(method_call, [this](C<MethodCall>& method_call) noexcept -> DesiredNodeType* {
        if (auto* res = boost::apply_visitor(*this, method_call.expression)) { return res; }
        return operator()(method_call.method);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<InitializerList>& init_list) const noexcept
{
    return match<InitializerList>(init_list, [this](C<InitializerList>& init_list) noexcept -> DesiredNodeType* {
        return visit_range(init_list.expressions);
    });
}

template<typename DesiredNodeType>
template<typename CurrentNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::match(C<CurrentNodeType>& current_node,
                                                      AdvanceAction<CurrentNodeType> advance_acation) const noexcept
{
    if constexpr (std::is_same_v<DesiredNodeTypeNoConst, CurrentNodeType>) {
        return predicate(current_node) ? &current_node : advance_acation(current_node);
    } else {
        return advance_acation(current_node);
    }
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::visit_range(C<std::vector<Expression>>& expressions) const noexcept
{
    for (auto& e : expressions) {
        if (auto* res = boost::apply_visitor(*this, e)) { return res; }
    }
    return nullptr;
}

}

#endif
