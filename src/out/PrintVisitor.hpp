#ifndef MAG_OUT_PRINTVISITOR_HPP
#define MAG_OUT_PRINTVISITOR_HPP

#include "out/AbstractSyntaxTree.hpp"

#include <iostream>

namespace MAG::Out {

class PrintVisitor final {
  private:
    std::ostream& out_stream;

  public:
    explicit PrintVisitor(std::ostream& out_stream = std::cout) noexcept;

    void apply_to_all(const std::vector<Statement>& ast) const noexcept;

    void operator()(const Assignment& assignment) const noexcept;
    void operator()(const VoidFunctionCall& void_function_call) const noexcept;
    void operator()(const FunctionDefinition& function_definition) const noexcept;
    void operator()(const ClassDefinition& class_definition) const noexcept;
    void operator()(const StructDefinition& struct_definition) const noexcept;
    void operator()(const ConstructorDefinition& constructor) const noexcept;
    void operator()(const ConstructorCall& constructor_call) const noexcept;
    void operator()(const Visibility& visibility) const noexcept;
    void operator()(const PreProcessor& pre_processor) const noexcept;
    void operator()(const Return& return_statement) const noexcept;
    void operator()(const ExpressionStatement& expr_statement) const noexcept;
    void operator()(const IfStatement& if_statement) const noexcept;
    void operator()(const ForStatement& for_statement) const noexcept;
    void operator()(const WhileStatement& while_statement) const noexcept;

    void operator()(const VariableDeclaration& variable_declaration) const noexcept;
    void operator()(const PrefixOperation& prefix_operation) const noexcept;
    void operator()(const BinaryOperation& binary_operation) const noexcept;
    void operator()(const Lambda& lambda) const noexcept;
    void operator()(const LambdaCaptureDeclaration& capture_declaration) const noexcept;
    void operator()(const LambdaCall& lambda_call) const noexcept;
    void operator()(const FunctionCall& function_call) const noexcept;
    void operator()(const VariableReference& variable_reference) const noexcept;
    void operator()(const StructuredBiding& binding) const noexcept;
    void operator()(const Literal& literal) const noexcept;
    void operator()(const StringLiteral& string_literal) const noexcept;
    void operator()(const MethodCall& method_call) const noexcept;
    void operator()(const InitializerList& init_list) const noexcept;

  private:
    void visit_range(const std::vector<Expression>& expressions) const noexcept;
};

}

#endif
