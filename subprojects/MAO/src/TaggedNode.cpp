#include "MAO/TaggedNode.hpp"

#include "MAO/Visitor.hpp"

#include <cassert>
#include <type_traits>

namespace MAO {

bool TaggedNodeEqualTo::operator()(TaggedNode lhs, TaggedNode rhs) const noexcept
{
    return std::visit(
        [rhs](auto lhs) noexcept -> bool {
            using T                       = decltype(lhs);
            const auto* const rhs_ref_ptr = std::get_if<T>(&rhs);
            if (!rhs_ref_ptr) { return false; }
            if constexpr (std::is_same_v<T, ArrayVariableLength>) {
                return &lhs.get_variable_declaration() == &rhs_ref_ptr->get_variable_declaration();
            } else {
                const auto* const lhs_ptr = &lhs.get();
                return &rhs_ref_ptr->get() == lhs_ptr;
            }
        },
        lhs);
}

std::size_t TaggedNodeHasher::operator()(TaggedNode tagged_node) const noexcept
{
    return std::hash<const void*>{}(
        std::visit(Visitor{[](ArrayVariableLength array_length) noexcept -> const void* {
                               return &array_length.get_variable_declaration();
                           },
                           [](auto node_ref) noexcept -> const void* { return &node_ref.get(); }},
                   tagged_node));
}

std::ostream& operator<<(std::ostream& os, TaggedNode tagged_node) noexcept
{
    std::visit(Visitor{
                   [&os](const MAL::AST::VariableDeclaration& variable_declaration) noexcept -> void {
                       os << variable_declaration.variable_name;
                   },
                   [&os](const MAL::AST::MultiplicationExpression::Element& mul_elem) noexcept -> void {
                       os << mul_elem.multiplication_operator;
                   },
                   [&os](const MAL::AST::AdditionExpression::Element& add_elem) noexcept -> void {
                       os << add_elem.addition_operator;
                   },
                   [&os](const MAL::AST::ComparisonExpression::Element& cmp_elem) noexcept -> void {
                       os << cmp_elem.comparison_operator;
                   },
                   [&os](const MAL::AST::BooleanOrExpression::Element& or_elem) noexcept -> void {
                       os << or_elem.logical_operator;
                   },
                   [&os](const MAL::AST::BooleanAndExpression::Element& and_elem) noexcept -> void {
                       os << and_elem.logical_operator;
                   },
                   [&os](const MAL::AST::NumericLiteral& num_lit) noexcept -> void {
                       boost::apply_visitor(
                           Visitor{[&os](bool value) noexcept -> void { os << std::boolalpha << value; },
                                   [&os](auto value) noexcept -> void { os << value; }},
                           num_lit);
                   },
                   [&os](const ArrayVariableLength& variable_len) noexcept -> void {
                       os << "len(" << variable_len.get_variable_declaration().variable_name << ')';
                   },
               },
               tagged_node);
    return os;
}

TaggedNodeExtractor::TaggedNodeExtractor() noexcept
{
    static constexpr std::size_t reserve_size{32};
    result.reserve(reserve_size);
}

void TaggedNodeExtractor::operator()(const MAL::AST::Assignment& assignment) noexcept
{
    boost::apply_visitor(
        Visitor{
            [this](const MAL::AST::VariableDeclaration& var_decl) noexcept -> void {
                [[maybe_unused]] const auto [it, inserted] = result.push_back(var_decl);
                assert(inserted);
            },
            [this](const MAL::AST::VariableReference& var_ref) noexcept -> void { operator()(var_ref); },
        },
        assignment.left_side);

    boost::apply_visitor(Visitor{
                             [this](const MAL::AST::BooleanOrExpression& boolean_or_expr) noexcept -> void {
                                 operator()(boolean_or_expr);
                             },
                             [this](const MAL::AST::ArrayLiteral& array_literal) noexcept -> void {
                                 operator()(array_literal.value);
                                 operator()(array_literal.size);
                             },
                         },
                         assignment.right_side);
}

void TaggedNodeExtractor::operator()(const MAL::AST::Send& send) noexcept
{
    for (const auto& var_ref : send) { operator()(var_ref); }
}

void TaggedNodeExtractor::operator()(const MAL::AST::Receive& receive) noexcept
{
    for (const auto& var_decl : receive) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(var_decl);
        assert(inserted);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::Print& /*print*/) noexcept
{
    assert(false); // Print statement should never be a part of application MAL code.
    std::terminate();
}

void TaggedNodeExtractor::operator()(const MAL::AST::IfStatement& if_statement) noexcept
{
    operator()(if_statement.condition);
    apply_to_all(if_statement.then_statements);
    apply_to_all(if_statement.else_statements);
}

void TaggedNodeExtractor::operator()(const MAL::AST::ForStatement& for_statement) noexcept
{
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    operator()(for_statement.condition);
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }
    apply_to_all(for_statement.body);
}

void TaggedNodeExtractor::operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    operator()(or_expr.first_expression);
    for (const auto& elem : or_expr.other_expressions) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(elem);
        assert(inserted);
        operator()(elem.boolean_and_expression);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept
{
    operator()(and_expr.first_expression);
    for (const auto& elem : and_expr.other_expressions) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(elem);
        assert(inserted);
        operator()(elem.comparison_expression);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept
{
    operator()(comp_expr.first_expression);
    for (const auto& elem : comp_expr.other_expressions) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(elem);
        assert(inserted);
        operator()(elem.addition_expression);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::AdditionExpression& add_expr) noexcept
{
    operator()(add_expr.first_expression);
    for (const auto& elem : add_expr.other_expressions) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(elem);
        assert(inserted);
        operator()(elem.multiplication_expression);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept
{
    operator()(mul_expr.first_expression);
    for (const auto& elem : mul_expr.other_expressions) {
        [[maybe_unused]] const auto [it, inserted] = result.push_back(elem);
        assert(inserted);
        operator()(elem.prefix_expression);
    }
}

void TaggedNodeExtractor::operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    boost::apply_visitor(
        Visitor{
            [this](const MAL::AST::NumericLiteral& num_lit) noexcept -> void {
                [[maybe_unused]] const auto [it, inserted] = result.push_back(num_lit);
                assert(inserted);
            },
            [this](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> void {
                result.push_back(ArrayVariableLength{len_fn_call});
            },
            [this](const MAL::AST::VariableReference& var_ref) noexcept -> void { operator()(var_ref); },
        },
        pref_expr.primary_expression);
}

void TaggedNodeExtractor::operator()(const MAL::AST::VariableReference& var_ref) noexcept
{
    if (var_ref.index) { operator()(*var_ref.index); }
}

void TaggedNodeExtractor::apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    for (const auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

TaggedNodeVector extract_tagged_nodes(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    TaggedNodeExtractor extractor;
    extractor.apply_to_all(ast);
    return std::move(extractor.result);
}

TaggedNode get_top_level_tagged_node(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    return or_expr.other_expressions.empty() ? get_top_level_tagged_node(or_expr.first_expression) :
                                               or_expr.other_expressions.back();
}

TaggedNode get_top_level_tagged_node(const MAL::AST::BooleanAndExpression& ae) noexcept
{
    return ae.other_expressions.empty() ? get_top_level_tagged_node(ae.first_expression) : ae.other_expressions.back();
}

TaggedNode get_top_level_tagged_node(const MAL::AST::ComparisonExpression& ce) noexcept
{
    return ce.other_expressions.empty() ? get_top_level_tagged_node(ce.first_expression) : ce.other_expressions.back();
}

TaggedNode get_top_level_tagged_node(const MAL::AST::AdditionExpression& ae) noexcept
{
    return ae.other_expressions.empty() ? get_top_level_tagged_node(ae.first_expression) : ae.other_expressions.back();
}

TaggedNode get_top_level_tagged_node(const MAL::AST::MultiplicationExpression& me) noexcept
{
    return me.other_expressions.empty() ? get_top_level_tagged_node(me.first_expression) : me.other_expressions.back();
}

TaggedNode get_top_level_tagged_node(const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    return boost::apply_visitor(
        Visitor{
            [](const MAL::AST::NumericLiteral& num_lit) noexcept -> TaggedNode { return num_lit; },
            [](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> TaggedNode {
                return ArrayVariableLength{len_fn_call};
            },
            [](const MAL::AST::VariableReference& var_ref) noexcept -> TaggedNode {
                assert(var_ref.variable_identifier.declaration);
                return *var_ref.variable_identifier.declaration;
            },
        },
        pref_expr.primary_expression);
}

std::optional<ConstrainedNode> to_constrained_node(TaggedNode tagged_node) noexcept
{
    return std::visit(
        Visitor{[](std::reference_wrapper<const MAL::AST::VariableDeclaration> var_decl) noexcept
                -> std::optional<ConstrainedNode> { return var_decl; },
                [](ArrayVariableLength arr_len) noexcept -> std::optional<ConstrainedNode> { return arr_len; },
                [](auto /*node*/) noexcept -> std::optional<ConstrainedNode> { return std::nullopt; }},
        tagged_node);
}

ConstrainedNodeVector get_constrained_nodes(const TaggedNodeVector& tagged_nodes) noexcept
{
    ConstrainedNodeVector result;
    result.reserve(tagged_nodes.size());
    for (auto tagged_node : tagged_nodes) {
        if (const auto constrained_node_opt = to_constrained_node(tagged_node)) {
            result.push_back(*constrained_node_opt);
        }
    }
    return result;
}

TaggedNode to_tagged_node(ConstrainedNode constrained_node) noexcept
{
    return std::visit(
        Visitor{[](std::reference_wrapper<const MAL::AST::VariableDeclaration> var_decl) noexcept -> TaggedNode {
                    return var_decl;
                },
                [](ArrayVariableLength arr_len) noexcept -> TaggedNode { return arr_len; }},
        constrained_node);
}

std::optional<std::size_t> get_node_index_if_present(const ConstrainedNodeVector& constrained_nodes,
                                                     TaggedNode tagged_node) noexcept
{
    if (const auto constrained_node = to_constrained_node(tagged_node)) {
        const auto node_it = constrained_nodes.find(*constrained_node);
        return node_it != constrained_nodes.cend() ?
                   std::make_optional<std::size_t>(std::distance(constrained_nodes.cbegin(), node_it)) :
                   std::nullopt;
    }
    return std::nullopt;
}

}
