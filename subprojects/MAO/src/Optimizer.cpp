#include "MAO/Optimizer.hpp"

#include "MAO/ExhaustiveSearchOptimizer.hpp"
#include "MAO/SSGAOptimizer.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cassert>

namespace MAO {

std::unique_ptr<Optimizer> Optimizer::create_from_name(std::string_view optimizer_name)
{
    static FactoryMap optimizer_factories{
        {
            "exhaustive",
            []() noexcept -> std::unique_ptr<Optimizer> { return std::make_unique<ExhaustiveSearchOptimizer>(); },
        },
        {
            "ssga",
            []() noexcept -> std::unique_ptr<Optimizer> { return std::make_unique<SSGAOptimizer>(); },
        },
    };

    const auto it = optimizer_factories.find(optimizer_name);
    if (it == optimizer_factories.cend()) { throw UnrecognizedOptimizerNameError{optimizer_name, optimizer_factories}; }
    return it->second();
}

ConstraintEvaluator::ConstraintEvaluator(const ConstrainedNodeVector& var_decls,
                                         const ConstraintVector& constraints,
                                         const HostVector& hosts) noexcept
        : intersecting_hosts{hosts.size()}
{
    index_constraints.reserve(constraints.size());
    for (const auto& constraint : constraints) {
        std::vector<std::size_t> index_constraint;
        index_constraint.reserve(constraint.get_elements().size());
        for (const auto& element : constraint.get_elements()) {
            const auto begin_it   = var_decls.begin();
            const auto element_it = var_decls.find(element);
            assert(element_it != var_decls.end());
            const auto index = static_cast<std::size_t>(std::distance(begin_it, element_it));
            index_constraint.push_back(index);
        }
        index_constraints.emplace_back(std::move(index_constraint), constraint.get_importance());
    }

    host_trusts.reserve(hosts.size());
    for (const auto& host : hosts) { host_trusts.push_back(host.get_trust()); }
}

double ConstraintEvaluator::evaluate(const Genotype& genotype)
{
    const auto n_hosts = host_trusts.size();
    assert(genotype.size() % n_hosts == 0);

    double risk{0.0};
    for (const auto& [index_constraint, importance] : index_constraints) {
        intersecting_hosts.set();
        for (const auto index : index_constraint) {
            const auto genotype_start_index = index * n_hosts;
            for (std::size_t i{0}; i < n_hosts; ++i) {
                intersecting_hosts[i] &= genotype.test(genotype_start_index + i);
            }
        }
        for (std::size_t i{0}; i < n_hosts; ++i) {
            if (intersecting_hosts.test(i)) { risk += static_cast<double>(importance) / host_trusts[i]; }
        }
    }
    return -risk;
}

UnrecognizedOptimizerNameError::UnrecognizedOptimizerNameError(
    std::string_view provided_optimizer_name, const Optimizer::FactoryMap& optimizer_factories) noexcept
        : std::runtime_error{format_what_message(provided_optimizer_name, optimizer_factories)},
          provided_optimizer_name{provided_optimizer_name},
          optimizer_factories{optimizer_factories}
{}

std::string_view UnrecognizedOptimizerNameError::get_provided_optimizer_name() const noexcept
{
    return provided_optimizer_name;
}

const Optimizer::FactoryMap& UnrecognizedOptimizerNameError::get_optimizer_factories() const noexcept
{
    return optimizer_factories;
}

std::string
UnrecognizedOptimizerNameError::format_what_message(std::string_view provided_optimizer_name,
                                                    const Optimizer::FactoryMap& optimizer_factories) noexcept
{
    assert(!optimizer_factories.empty());
    assert(optimizer_factories.find(provided_optimizer_name) == optimizer_factories.cend());

    std::ostringstream oss;
    oss << "Optimizer " << std::quoted(provided_optimizer_name)
        << " does not exist. Available optimizers: " << std::quoted(optimizer_factories.begin()->first);
    for (auto it = std::next(optimizer_factories.cbegin()); it != optimizer_factories.cend(); ++it) {
        oss << ", " << std::quoted(it->first);
    }
    oss << '.';
    return oss.str();
}

}
