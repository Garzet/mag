#include "MAO/Trio.hpp"

#include "MAO/Visitor.hpp"

#include <cassert>

namespace MAO {

bool Trio::MiddleNodeNodeEqualTo::operator()(MiddleNode lhs, MiddleNode rhs) const noexcept
{
    return std::visit(
        [rhs](auto lhs) noexcept -> bool {
            using T                       = decltype(lhs);
            const auto* const rhs_ref_ptr = std::get_if<T>(&rhs);
            if (!rhs_ref_ptr) { return false; }
            const auto* const lhs_ptr = &lhs.get();
            return &rhs_ref_ptr->get() == lhs_ptr;
        },
        lhs);
}

Trio::Trio(OuterNode first, MiddleNode second, OuterNode third) noexcept : first{first}, second{second}, third{third}
{
    assert(!TaggedNodeEqualTo{}(first, to_outer_node(second)));
    assert(!TaggedNodeEqualTo{}(first, third));
    assert(!TaggedNodeEqualTo{}(to_outer_node(second), third));
}

auto Trio::get_first() const noexcept -> Trio::OuterNode
{
    return first;
}

auto Trio::get_second() const noexcept -> Trio::MiddleNode
{
    return second;
}

auto Trio::get_third() const noexcept -> Trio::OuterNode
{
    return third;
}

std::tuple<Trio::OuterNode, Trio::MiddleNode, Trio::OuterNode> to_tuple(Trio trio) noexcept
{
    return {trio.get_first(), trio.get_second(), trio.get_third()};
}

std::optional<Trio::MiddleNode> to_middle_node(TaggedNode outer_node) noexcept
{
    return std::visit(
        Visitor{
            [](const MAL::AST::VariableDeclaration& /*node*/) noexcept -> std::optional<Trio::MiddleNode> {
                return std::nullopt;
            },
            [](const MAL::AST::MultiplicationExpression::Element& node) noexcept -> std::optional<Trio::MiddleNode> {
                return node;
            },
            [](const MAL::AST::AdditionExpression::Element& node) noexcept -> std::optional<Trio::MiddleNode> {
                return node;
            },
            [](const MAL::AST::ComparisonExpression::Element& node) noexcept -> std::optional<Trio::MiddleNode> {
                return node;
            },
            [](const MAL::AST::BooleanOrExpression::Element& node) noexcept -> std::optional<Trio::MiddleNode> {
                return node;
            },
            [](const MAL::AST::BooleanAndExpression::Element& node) noexcept -> std::optional<Trio::MiddleNode> {
                return node;
            },
            [](const MAL::AST::NumericLiteral& /*node*/) noexcept -> std::optional<Trio::MiddleNode> {
                return std::nullopt;
            },
            [](ArrayVariableLength /*node*/) noexcept -> std::optional<Trio::MiddleNode> { return std::nullopt; },
        },
        outer_node);
}

Trio::OuterNode to_outer_node(Trio::MiddleNode middle_node) noexcept
{
    return std::visit([](auto middle_node) noexcept -> TaggedNode { return TaggedNode{middle_node}; }, middle_node);
}

bool TrioEqualTo::operator()(Trio lhs, Trio rhs) const noexcept
{
    static constexpr TaggedNodeEqualTo tagged_node_equal_to;
    return tagged_node_equal_to(lhs.get_first(), rhs.get_first()) &&
           tagged_node_equal_to(to_outer_node(lhs.get_second()), to_outer_node(rhs.get_second())) &&
           tagged_node_equal_to(lhs.get_third(), rhs.get_third());
}

TrioExtractor::TrioExtractor() noexcept
{
    static constexpr std::size_t reserve_size{16};
    result.reserve(reserve_size);
}

void TrioExtractor::operator()(const MAL::AST::Assignment& assignment) noexcept
{
    boost::apply_visitor(
        Visitor{
            [this](const MAL::AST::BooleanOrExpression& or_expr) noexcept -> void { operator()(or_expr); },
            [this](const MAL::AST::ArrayLiteral& array_literal) noexcept -> void {
                operator()(array_literal.value);
                operator()(array_literal.size);
            },
        },
        assignment.right_side);
}

void TrioExtractor::operator()(const MAL::AST::Send& send) noexcept
{
    for (const auto& var_ref : send) {
        if (var_ref.index) { operator()(*var_ref.index); }
    }
}

void TrioExtractor::operator()(const MAL::AST::Receive& /*receive*/) noexcept {}

void TrioExtractor::operator()(const MAL::AST::Print& /*print*/) noexcept
{
    assert(false); // Print statement should never be a part of application MAL code.
    std::terminate();
}

void TrioExtractor::operator()(const MAL::AST::IfStatement& if_statement) noexcept
{
    operator()(if_statement.condition);
    apply_to_all(if_statement.then_statements);
    apply_to_all(if_statement.else_statements);
}

void TrioExtractor::operator()(const MAL::AST::ForStatement& for_statement) noexcept
{
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    operator()(for_statement.condition);
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }
    apply_to_all(for_statement.body);
}

void TrioExtractor::operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    operator()(or_expr.first_expression);
    for (const auto& [and_expr] : or_expr.other_expressions) { operator()(and_expr); }
}

void TrioExtractor::operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept
{
    operator()(and_expr.first_expression);
    for (const auto& [cmp_expr] : and_expr.other_expressions) { operator()(cmp_expr); }
}

void TrioExtractor::operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept
{
    auto first = operator()(comp_expr.first_expression);
    for (const auto& el : comp_expr.other_expressions) {
        const Trio::MiddleNode second{el};
        const auto third = operator()(el.addition_expression);
        if (!TaggedNodeEqualTo{}(first, third)) {
            [[maybe_unused]] const auto [it, inserted] = result.push_back(Trio{first, second, third});
            assert(inserted);
        }
        first = el;
    }
}

Trio::OuterNode TrioExtractor::operator()(const MAL::AST::AdditionExpression& add_expr) noexcept
{
    auto first = operator()(add_expr.first_expression);
    for (const auto& el : add_expr.other_expressions) {
        const Trio::MiddleNode second{el};
        const auto third = operator()(el.multiplication_expression);
        if (!TaggedNodeEqualTo{}(first, third)) {
            [[maybe_unused]] const auto [it, inserted] = result.push_back(Trio{first, second, third});
            assert(inserted);
        }
        first = el;
    }
    return first;
}

Trio::OuterNode TrioExtractor::operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept
{
    auto first = operator()(mul_expr.first_expression);
    for (const auto& el : mul_expr.other_expressions) {
        const Trio::MiddleNode second{el};
        const auto third = operator()(el.prefix_expression);
        if (!TaggedNodeEqualTo{}(first, third)) {
            [[maybe_unused]] const auto [it, inserted] = result.push_back(Trio{first, second, third});
            assert(inserted);
        }
        first = el;
    }
    return first;
}

Trio::OuterNode TrioExtractor::operator()(const MAL::AST::PrefixExpression& pref_expr) const noexcept
{
    return boost::apply_visitor(Visitor{[](const MAL::AST::VariableReference& var_ref) noexcept -> Trio::OuterNode {
                                            return Trio::OuterNode{*var_ref.variable_identifier.declaration};
                                        },
                                        [](const MAL::AST::NumericLiteral& num_lit) noexcept -> Trio::OuterNode {
                                            return Trio::OuterNode{num_lit};
                                        },
                                        [](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> Trio::OuterNode {
                                            return Trio::OuterNode{ArrayVariableLength{len_fn_call}};
                                        }},
                                pref_expr.primary_expression);
}

void TrioExtractor::apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    for (const auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

TrioVector extract_trios(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    TrioExtractor trio_extractor;
    trio_extractor.apply_to_all(ast);
    return std::move(trio_extractor.result);
}

}
