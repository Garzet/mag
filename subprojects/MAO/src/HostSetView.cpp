#include "MAO/HostSetView.hpp"

namespace MAO {

HostSetView::HostSetView(Genotype& genotype, std::size_t n_hosts, std::size_t node_index) noexcept
        : genotype{genotype}, n_hosts{n_hosts}, node_index{node_index}
{}

bool HostSetView::test(std::size_t host) const noexcept
{
    assert(host < n_hosts);

    return genotype.get().test(node_index * n_hosts + host);
}

void HostSetView::set(std::size_t host, bool value) const noexcept
{
    genotype.get().set(node_index * n_hosts + host, value);
}

std::size_t HostSetView::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t HostSetView::get_node_index() const noexcept
{
    return node_index;
}

bool HostSetView::is_subset_of(HostSetView other) const noexcept
{
    assert(&genotype.get() == &other.genotype.get());
    assert(n_hosts == other.n_hosts);

    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (test(h) && !other.test(h)) { return false; }
    }
    return true;
}

bool HostSetView::is_superset_of(HostSetView other) const noexcept
{
    assert(&genotype.get() == &other.genotype.get());
    assert(n_hosts == other.n_hosts);

    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (!test(h) && other.test(h)) { return false; }
    }
    return true;
}

std::size_t HostSetView::count_hosts() const noexcept
{
    std::size_t result{0};
    for (std::size_t h{0}; h < n_hosts; ++h) { result += test(h); }
    return result;
}

bool HostSetView::operator==(HostSetView other) const noexcept
{
    assert(&genotype.get() == &other.genotype.get());
    assert(n_hosts == other.n_hosts);

    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (test(h) != other.test(h)) { return false; }
    }
    return true;
}

bool HostSetView::operator!=(HostSetView other) const noexcept
{
    return !(*this == other);
}

HostSetConstView::HostSetConstView(const Genotype& genotype, std::size_t n_hosts, std::size_t node_index) noexcept
        : genotype{genotype}, n_hosts{n_hosts}, node_index{node_index}
{}

bool HostSetConstView::test(std::size_t host) const noexcept
{
    assert(host < n_hosts);

    return genotype.get().test(node_index * n_hosts + host);
}

std::size_t HostSetConstView::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t HostSetConstView::get_node_index() const noexcept
{
    return node_index;
}

std::size_t HostSetConstView::get_number_of_node_hosts() const noexcept
{
    std::size_t result{0};
    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (test(h)) { ++result; }
    }
    return result;
}

bool is_intersection_empty(HostSetConstView lhs, HostSetConstView rhs) noexcept
{
    assert(lhs.get_n_hosts() == rhs.get_n_hosts());
    assert(lhs.get_node_index() != rhs.get_node_index());

    for (std::size_t h{0}; h < lhs.get_n_hosts(); ++h) {
        if (lhs.test(h) && rhs.test(h)) { return false; }
    }
    return true;
}

HostSetComputationCache::HostSetComputationCache(std::size_t n_hosts) noexcept : cache{n_hosts}
{
    assert(n_hosts > 0);
}

const HostSetComputationCache& HostSetComputationCache::compute_intersection(HostSetView a, HostSetView b) noexcept
{
    const auto n_hosts = get_n_hosts();
    assert(n_hosts == a.get_n_hosts());
    assert(n_hosts == b.get_n_hosts());
    for (std::size_t h{0}; h < n_hosts; ++h) { cache.set(h, a.test(h) && b.test(h)); }

    return *this;
}

const HostSetComputationCache&
HostSetComputationCache::compute_intersection(HostSetView a, HostSetView b, HostSetView c) noexcept
{
    compute_intersection(a, b);
    intersect_cache_with_host_set(c);

    return *this;
}

const HostSetComputationCache& HostSetComputationCache::compute_union(HostSetView a, HostSetView b) noexcept
{
    const auto n_hosts = get_n_hosts();
    assert(n_hosts == a.get_n_hosts());
    assert(n_hosts == b.get_n_hosts());
    for (std::size_t h{0}; h < n_hosts; ++h) { cache.set(h, a.test(h) || b.test(h)); }

    return *this;
}

const HostSetComputationCache&
HostSetComputationCache::compute_union(HostSetView a, HostSetView b, HostSetView c) noexcept
{
    compute_union(a, b);
    union_cache_with_host_set(c);

    return *this;
}

bool HostSetComputationCache::all() const noexcept
{
    return cache.all();
}

bool HostSetComputationCache::none() const noexcept
{
    return cache.none();
}

bool HostSetComputationCache::test(std::size_t host) const noexcept
{
    return cache.test(host);
}

std::size_t HostSetComputationCache::get_n_hosts() const noexcept
{
    return cache.size();
}

void HostSetComputationCache::intersect_cache_with_host_set(HostSetView host_set) noexcept
{
    const auto n_hosts = get_n_hosts();
    assert(n_hosts == host_set.get_n_hosts());
    for (std::size_t h{0}; h < n_hosts; ++h) { cache.set(h, cache.test(h) && host_set.test(h)); }
}

void HostSetComputationCache::union_cache_with_host_set(HostSetView host_set) noexcept
{
    const auto n_hosts = get_n_hosts();
    assert(n_hosts == host_set.get_n_hosts());
    for (std::size_t h{0}; h < n_hosts; ++h) { cache.set(h, cache.test(h) || host_set.test(h)); }
}

}
