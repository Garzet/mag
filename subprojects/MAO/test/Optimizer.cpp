#include <boost/test/unit_test.hpp>

#include "MAO/ExhaustiveSearchOptimizer.hpp"

#include <unordered_set>

BOOST_AUTO_TEST_SUITE(optimizer);

BOOST_AUTO_TEST_SUITE(advancement);

namespace {

[[nodiscard]] std::size_t count_unique_solutions(std::size_t expected_number_of_unique_solutions,
                                                 MAO::ExhaustiveSearchOptimizer::SimpleAdvancement advancement) noexcept
{
    const auto n_hosts             = advancement.get_n_hosts();
    const auto n_nodes_to_optimize = advancement.get_n_nodes_to_optimize();

    std::unordered_set<MAO::Genotype> unique_solutions;
    unique_solutions.reserve(expected_number_of_unique_solutions);

    MAO::Genotype genotype{n_nodes_to_optimize * n_hosts};
    for (std::size_t n{0}; n < n_nodes_to_optimize; ++n) { genotype.set(n * n_hosts); }
    do {
        unique_solutions.insert(genotype);
    } while (advancement.advance(genotype));

    return unique_solutions.size();
}

}

BOOST_AUTO_TEST_CASE(first_advancement_results_in_first_variable_on_the_second_host)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int, var b int;
         send a, b;
     )"};
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
        {"host3", 30},
    };
    const auto app_ast             = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes        = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes   = MAO::get_constrained_nodes(tagged_nodes);
    const auto n_constrained_nodes = constrained_nodes.size();
    const auto n_hosts             = hosts.size();
    BOOST_REQUIRE_EQUAL(n_constrained_nodes, 2);

    MAO::Genotype genotype{n_constrained_nodes * n_hosts};
    for (std::size_t n{0}; n < n_constrained_nodes; ++n) { genotype.set(n * n_hosts); }

    const auto equivalences             = MAO::extract_equivalences(app_ast);
    const auto indexing_information     = MAO::extract_indexing_information(app_ast, constrained_nodes);
    const auto trio                     = MAO::extract_trios(app_ast);
    const auto flow_control_information = MAO::extract_flow_control_information(app_ast);
    MAO::ExhaustiveSearchOptimizer::SimpleAdvancement advancement{
        hosts, constrained_nodes, equivalences, indexing_information, trio, flow_control_information};

    const auto more_advancements_available = advancement.advance(genotype);

    BOOST_CHECK(more_advancements_available);
    BOOST_CHECK_EQUAL(genotype, MAO::Genotype{std::string{"001010"}});
}

BOOST_AUTO_TEST_CASE(four_nodes_with_no_indexing_and_no_trios_on_two_hosts_result_in_eighty_one_unique_solutions)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var c int, var d int;
        send d;
    )"};
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
    };
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);
    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 4);

    const auto equivalences             = MAO::extract_equivalences(app_ast);
    const auto indexing_information     = MAO::extract_indexing_information(app_ast, constrained_nodes);
    const auto trio                     = MAO::extract_trios(app_ast);
    const auto flow_control_information = MAO::extract_flow_control_information(app_ast);
    MAO::ExhaustiveSearchOptimizer::SimpleAdvancement advancement{
        hosts, constrained_nodes, equivalences, indexing_information, trio, flow_control_information};

    static constexpr std::size_t expected_n_unique_solutions{81};
    const auto n_unique_solutions = count_unique_solutions(expected_n_unique_solutions, std::move(advancement));
    BOOST_CHECK_EQUAL(n_unique_solutions, expected_n_unique_solutions);
}

BOOST_AUTO_TEST_CASE(two_nodes_with_single_indexing_and_no_trios_on_two_hosts_result_in_five_unique_solutions)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var i int;
        send a[i];
    )"};
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
    };
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);
    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 2);

    const auto equivalences             = MAO::extract_equivalences(app_ast);
    const auto indexing_information     = MAO::extract_indexing_information(app_ast, constrained_nodes);
    const auto trio                     = MAO::extract_trios(app_ast);
    const auto flow_control_information = MAO::extract_flow_control_information(app_ast);
    MAO::ExhaustiveSearchOptimizer::SimpleAdvancement advancement{
        hosts, constrained_nodes, equivalences, indexing_information, trio, flow_control_information};

    static constexpr std::size_t expected_n_unique_solutions{5};
    const auto n_unique_solutions = count_unique_solutions(expected_n_unique_solutions, std::move(advancement));
    BOOST_CHECK_EQUAL(n_unique_solutions, expected_n_unique_solutions);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(evaluation);

namespace {

constexpr double comparison_tolerance_in_percent{0.0001};

}

BOOST_AUTO_TEST_CASE(no_violated_constraint_results_in_zero_risk)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int, var b int, var c int;
         send c;
     )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);
    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 3);
    const auto a = constrained_nodes[0];
    const auto b = constrained_nodes[1];
    const auto c = constrained_nodes[2];
    const MAO::ConstraintVector constraints{
        {{a, b}, 100},
        {{a, c}, 100},
        {{b, c}, 100},
    };
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
        {"host3", 30},
    };

    MAO::Genotype genotype{std::string{"100010001"}};
    MAO::ConstraintEvaluator evaluator{constrained_nodes, constraints, hosts};
    const auto fitness = evaluator.evaluate(genotype);

    BOOST_CHECK_CLOSE(fitness, 0.0, comparison_tolerance_in_percent);
}

BOOST_AUTO_TEST_CASE(contraint_violated_on_two_hosts_results_in_expected_risk)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int, var b int, var c int;
         send c;
     )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);
    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 3);
    const auto a = constrained_nodes[0];
    const auto b = constrained_nodes[1];
    const MAO::ConstraintVector constraints{
        {{a, b}, 100},
    };
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
    };

    MAO::Genotype genotype{tagged_nodes.size() * hosts.size()};
    genotype.set();
    MAO::ConstraintEvaluator evaluator{constrained_nodes, constraints, hosts};
    const auto fitness = evaluator.evaluate(genotype);

    BOOST_CHECK_CLOSE(fitness, -15.0, comparison_tolerance_in_percent);
}

BOOST_AUTO_TEST_CASE(two_contraints_violated_on_two_hosts_results_in_expected_risk)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int, var b int, var c int;
         send a, b, c;
     )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);
    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 3);
    const auto a = constrained_nodes[0];
    const auto b = constrained_nodes[1];
    const auto c = constrained_nodes[2];
    const MAO::ConstraintVector constraints{
        {{a, b}, 100},
        {{a, c}, 90},
    };
    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
        {"host3", 30},
    };

    MAO::Genotype genotype{std::string{"100001101"}};
    MAO::ConstraintEvaluator evaluator{constrained_nodes, constraints, hosts};
    const auto fitness = evaluator.evaluate(genotype);

    BOOST_CHECK_CLOSE(fitness, -13.0, comparison_tolerance_in_percent);
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
