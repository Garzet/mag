#include <boost/test/unit_test.hpp>

#include "MAO/Trio.hpp"

BOOST_AUTO_TEST_SUITE(trio_extraction);

BOOST_AUTO_TEST_CASE(code_with_single_multiplication_expression_results_in_a_single_trio)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int;
        var c int = a[42] * b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto trios = MAO::extract_trios(app_ast);

    BOOST_REQUIRE_EQUAL(trios.size(), 1);
    const auto [first, second, third] = MAO::to_tuple(trios.front());
    {
        const auto* const decl = std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&first);
        BOOST_REQUIRE(decl);
        BOOST_CHECK_EQUAL(decl->get().variable_name, "a");
    }
    {
        const auto* const el =
            std::get_if<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>>(&second);
        BOOST_CHECK(el);
    }
    {
        const auto* const decl = std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&third);
        BOOST_REQUIRE(decl);
        BOOST_CHECK_EQUAL(decl->get().variable_name, "b");
    }
}

BOOST_AUTO_TEST_CASE(code_with_same_node_in_multiplication_expression_results_in_no_trios)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int;
        var b int = a * a;
        send b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto trios = MAO::extract_trios(app_ast);

    BOOST_CHECK(trios.empty());
}

BOOST_AUTO_TEST_CASE(code_with_multiplication_and_addition_results_in_two_trios)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var d int;
        var c int = a * b + d;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto trios = MAO::extract_trios(app_ast);

    BOOST_REQUIRE_EQUAL(trios.size(), 2);
    {
        const auto [first, second, third] = MAO::to_tuple(trios[0]);
        {
            const auto* const decl = std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&first);
            BOOST_REQUIRE(decl);
            BOOST_CHECK_EQUAL(decl->get().variable_name, "a");
        }
        {
            const auto* const el =
                std::get_if<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>>(&second);
            BOOST_CHECK(el);
        }
        {
            const auto* const decl = std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&third);
            BOOST_REQUIRE(decl);
            BOOST_CHECK_EQUAL(decl->get().variable_name, "b");
        }
    }
    {
        const auto [first, second, third] = MAO::to_tuple(trios[1]);
        {
            const auto* const el =
                std::get_if<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>>(&first);
            BOOST_CHECK(el);
        }
        {
            const auto* const el =
                std::get_if<std::reference_wrapper<const MAL::AST::AdditionExpression::Element>>(&second);
            BOOST_CHECK(el);
        }
        {
            const auto* const decl = std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&third);
            BOOST_REQUIRE(decl);
            BOOST_CHECK_EQUAL(decl->get().variable_name, "d");
        }
    }
}

BOOST_AUTO_TEST_CASE(code_with_boolean_expressions_results_in_no_trios)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a bool, var b bool;
        var c bool = a && b;
        var d bool = a || b;
        send c, d;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto trios = MAO::extract_trios(app_ast);

    BOOST_CHECK(trios.empty());
}

BOOST_AUTO_TEST_SUITE_END();
