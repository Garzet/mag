#include <boost/test/unit_test.hpp>

#include "MAO/FlowControl.hpp"

BOOST_AUTO_TEST_SUITE(flow_control_information_extraction);

BOOST_AUTO_TEST_CASE(simple_if_statement_results_in_expected_flow_control_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int;
        var c int = 42;
        if (a > b) {
            send a;
        } else {
            send b;
        }
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto flow_control_info = MAO::extract_flow_control_information(app_ast);

    BOOST_REQUIRE_EQUAL(flow_control_info.size(), 1);
    const auto& [control_flow_node, affected_nodes] = *flow_control_info.begin();
    BOOST_REQUIRE(std::holds_alternative<std::reference_wrapper<const MAL::AST::ComparisonExpression::Element>>(
        control_flow_node));
    BOOST_REQUIRE_EQUAL(affected_nodes.size(), 2);
    {
        const auto* const a_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&affected_nodes[0]);
        BOOST_REQUIRE(a_decl);
        BOOST_CHECK_EQUAL(a_decl->get().variable_name, "a");
    }
    {
        const auto* const b_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&affected_nodes[1]);
        BOOST_REQUIRE(b_decl);
        BOOST_CHECK_EQUAL(b_decl->get().variable_name, "b");
    }
}

BOOST_AUTO_TEST_CASE(simple_for_statement_results_in_expected_flow_control_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        var power int = 0;
        for (var i int = 0; i < len(a); i = i + 1) {
            power = power * 2;
        }
        send power;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto flow_control_info = MAO::extract_flow_control_information(app_ast);

    BOOST_REQUIRE_EQUAL(flow_control_info.size(), 1);
    const auto& [control_flow_node, affected_nodes] = *flow_control_info.begin();
    BOOST_REQUIRE(std::holds_alternative<std::reference_wrapper<const MAL::AST::ComparisonExpression::Element>>(
        control_flow_node));
    BOOST_REQUIRE_EQUAL(affected_nodes.size(), 3);
    {
        const auto* const power_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&affected_nodes[0]);
        BOOST_REQUIRE(power_decl);
        BOOST_CHECK_EQUAL(power_decl->get().variable_name, "power");
    }
    {
        BOOST_REQUIRE(std::holds_alternative<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>>(
            affected_nodes[1]));
    }
    {
        const auto* const num_lit_2 =
            std::get_if<std::reference_wrapper<const MAL::AST::NumericLiteral>>(&affected_nodes[2]);
        BOOST_REQUIRE(num_lit_2);
        const auto* const num_lit_2_as_int = boost::get<int>(&num_lit_2->get());
        BOOST_REQUIRE(num_lit_2_as_int);
        BOOST_CHECK_EQUAL(*num_lit_2_as_int, 2);
    }
}

BOOST_AUTO_TEST_CASE(nested_flow_control_statements_results_in_expected_flow_control_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        var count int = 0;
        for (var i int = 0; i < len(a); i = i + 1) {
            if (a[i] == 42) {
                count = count + 1;
            }
        }
        send count;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto flow_control_info = MAO::extract_flow_control_information(app_ast);

    const auto find_comparison_element_in_flow_control_info =
        [&flow_control_info](MAL::AST::ComparisonOperator op) noexcept -> MAO::FlowControlInformation::const_iterator {
        return std::find_if(flow_control_info.cbegin(),
                            flow_control_info.cend(),
                            [op](const MAO::FlowControlInformation::value_type& element) noexcept -> bool {
                                const auto* const cmp_elem =
                                    std::get_if<std::reference_wrapper<const MAL::AST::ComparisonExpression::Element>>(
                                        &element.first);
                                if (!cmp_elem) { return false; }
                                return cmp_elem->get().comparison_operator == op;
                            });
    };

    BOOST_REQUIRE_EQUAL(flow_control_info.size(), 2);

    {
        const auto for_cmp_expr_it = find_comparison_element_in_flow_control_info(MAL::AST::ComparisonOperator::Less);
        BOOST_REQUIRE((for_cmp_expr_it != flow_control_info.cend()));
        const auto& affected_nodes = for_cmp_expr_it->second;
        BOOST_CHECK_EQUAL(affected_nodes.size(), 7);
    }

    {
        const auto if_cmp_expr_it = find_comparison_element_in_flow_control_info(MAL::AST::ComparisonOperator::Equal);
        BOOST_REQUIRE((if_cmp_expr_it != flow_control_info.cend()));
        const auto& affected_nodes = if_cmp_expr_it->second;
        BOOST_CHECK_EQUAL(affected_nodes.size(), 3);
    }
}

BOOST_AUTO_TEST_SUITE_END();
