#ifndef MAO_EXHAUSTIVESEARCHOPTIMIZER_HPP
#define MAO_EXHAUSTIVESEARCHOPTIMIZER_HPP

#include "MAO/Optimizer.hpp"

#include <SHO/SO/ExhaustiveSearch.hpp>

namespace MAO {

class ExhaustiveSearchOptimizer final : public Optimizer {
  public:
    using Algorithm = SHO::SO::ExhaustiveSearch<Genotype>;

    class SimpleAdvancement final : public Algorithm::Advancement {
      private:
        TransformerTuple transformers;
        std::optional<Genotype> non_transformed_genotype;

      public:
        SimpleAdvancement(const HostVector& hosts,
                          const ConstrainedNodeVector& nodes_to_optimize,
                          const EquivalenceMap& equivalences,
                          const IndexingInformation& indexing_information,
                          const TrioVector& trios,
                          const FlowControlInformation& flow_control_informatio) noexcept;

        [[nodiscard]] bool advance(Genotype& genotype) final;

        [[nodiscard]] std::size_t get_n_hosts() const noexcept;
        [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;
    };

  public:
    [[nodiscard]] Genotype optimize(const Configuration& configuration) final;
};

}

#endif
