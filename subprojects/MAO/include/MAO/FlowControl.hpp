#ifndef MAO_FLOWCONTROL_HPP
#define MAO_FLOWCONTROL_HPP

#include "MAO/TaggedNode.hpp"

#include <unordered_map>
#include <functional>
#include <vector>

namespace MAO {

using FlowControlInformation = std::unordered_map<TaggedNode, TaggedNodeVector, TaggedNodeHasher, TaggedNodeEqualTo>;

class FlowControlInformationExtractor final {
  public:
    FlowControlInformation result;

  private:
    TaggedNodeVector running_condition_nodes;

  public:
    FlowControlInformationExtractor() noexcept;

    void operator()(const MAL::AST::Assignment& assignment) noexcept;
    void operator()(const MAL::AST::Send& send) noexcept;
    void operator()(const MAL::AST::Receive& receive) noexcept;
    void operator()(const MAL::AST::Print& print) noexcept;
    void operator()(const MAL::AST::IfStatement& if_statement) noexcept;
    void operator()(const MAL::AST::ForStatement& for_statement) noexcept;

    void operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
    void operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept;
    void operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept;
    void operator()(const MAL::AST::AdditionExpression& add_expr) noexcept;
    void operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept;
    void operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept;

    void operator()(const MAL::AST::VariableReference& var_ref) noexcept;

    void add_for_all_running_condition_nodes(TaggedNode node) noexcept;

    void apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept;
};

[[nodiscard]] FlowControlInformation
extract_flow_control_information(const std::vector<MAL::AST::Statement>& ast) noexcept;

}

#endif
