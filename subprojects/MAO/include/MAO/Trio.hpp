#ifndef MAO_TRIO_HPP
#define MAO_TRIO_HPP

#include "MAO/UniqueValueVector.hpp"
#include "MAO/TaggedNode.hpp"

#include <MAL.hpp>

#include <variant>
#include <functional>
#include <tuple>
#include <optional>

namespace MAO {

class Trio final {
  public:
    using OuterNode  = TaggedNode;
    using MiddleNode = std::variant<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>,
                                    std::reference_wrapper<const MAL::AST::AdditionExpression::Element>,
                                    std::reference_wrapper<const MAL::AST::ComparisonExpression::Element>,
                                    std::reference_wrapper<const MAL::AST::BooleanOrExpression::Element>,
                                    std::reference_wrapper<const MAL::AST::BooleanAndExpression::Element>>;

    struct MiddleNodeNodeEqualTo final {
        [[nodiscard]] bool operator()(MiddleNode lhs, MiddleNode rhs) const noexcept;
    };

  private:
    OuterNode first;
    MiddleNode second;
    OuterNode third;

  public:
    Trio(OuterNode first, MiddleNode second, OuterNode third) noexcept;

    [[nodiscard]] OuterNode get_first() const noexcept;
    [[nodiscard]] MiddleNode get_second() const noexcept;
    [[nodiscard]] OuterNode get_third() const noexcept;
};

[[nodiscard]] std::tuple<Trio::OuterNode, Trio::MiddleNode, Trio::OuterNode> to_tuple(Trio trio) noexcept;

[[nodiscard]] Trio::OuterNode to_outer_node(Trio::MiddleNode middle_node) noexcept;
[[nodiscard]] std::optional<Trio::MiddleNode> to_middle_node(TaggedNode outer_node) noexcept;

struct TrioEqualTo final {
    [[nodiscard]] bool operator()(Trio lhs, Trio rhs) const noexcept;
};

using TrioVector = UniqueValueVector<Trio, TrioEqualTo>;

class TrioExtractor final {
  public:
    TrioVector result;

  public:
    TrioExtractor() noexcept;

    void operator()(const MAL::AST::Assignment& assignment) noexcept;
    void operator()(const MAL::AST::Send& send) noexcept;
    void operator()(const MAL::AST::Receive& receive) noexcept;
    void operator()(const MAL::AST::Print& print) noexcept;
    void operator()(const MAL::AST::IfStatement& if_statement) noexcept;
    void operator()(const MAL::AST::ForStatement& for_statement) noexcept;

    void operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
    void operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept;
    void operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept;
    Trio::OuterNode operator()(const MAL::AST::AdditionExpression& add_expr) noexcept;
    [[nodiscard]] Trio::OuterNode operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept;
    [[nodiscard]] Trio::OuterNode operator()(const MAL::AST::PrefixExpression& pref_expr) const noexcept;

    void apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept;
};

[[nodiscard]] TrioVector extract_trios(const std::vector<MAL::AST::Statement>& ast) noexcept;

}

#endif
