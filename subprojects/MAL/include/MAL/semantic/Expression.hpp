#ifndef MAL_SEMANTIC_EXPRESSION_HPP
#define MAL_SEMANTIC_EXPRESSION_HPP

#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL {

class ExpressionTypeSetter final {
  public:
    void operator()(MAL::AST::Assignment& assignment) const;
    void operator()(MAL::AST::Send& send) const;
    void operator()(MAL::AST::Receive& receive) const noexcept;
    void operator()(MAL::AST::Print& print) const noexcept;
    void operator()(MAL::AST::IfStatement& if_statement) const;
    void operator()(MAL::AST::ForStatement& for_statement) const;

    void apply_to_all(std::vector<AST::Statement>& ast) const;
};

void determine_expression_types(std::vector<AST::Statement>& ast);

[[nodiscard]] AST::TypeKeyword determine_expression_types(AST::NumericLiteral& nl) noexcept;
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::PrimaryExpression& pe);
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::PrefixExpression& pe);
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::MultiplicationExpression& me);
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::AdditionExpression& ae);
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::ComparisonExpression& ce);
[[nodiscard]] AST::ExpressionTypeInformation determine_expression_types(AST::BooleanAndExpression& bae);
AST::ExpressionTypeInformation determine_expression_types(AST::BooleanOrExpression& boe);

class NonMatchingTypesInMultiplicationExpressionError final : public std::exception {
  public:
    const AST::MultiplicationExpression& multiplication_expression;

  public:
    explicit NonMatchingTypesInMultiplicationExpressionError(
        const AST::MultiplicationExpression& multiplication_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonMatchingTypesInMultiplicationExpressionError>);

class MultiplicationExpressionContainsArrayTypeError final : public std::exception {
  public:
    const AST::MultiplicationExpression& multiplication_expression;

  public:
    explicit MultiplicationExpressionContainsArrayTypeError(
        const AST::MultiplicationExpression& multiplication_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<MultiplicationExpressionContainsArrayTypeError>);

class NonMatchingTypesInAdditionExpressionError final : public std::exception {
  public:
    const AST::AdditionExpression& addition_expression;

  public:
    explicit NonMatchingTypesInAdditionExpressionError(const AST::AdditionExpression& addition_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonMatchingTypesInAdditionExpressionError>);

class AdditionExpressionContainsArrayTypeError final : public std::exception {
  public:
    const AST::AdditionExpression& addition_expression;

  public:
    explicit AdditionExpressionContainsArrayTypeError(const AST::AdditionExpression& addition_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<AdditionExpressionContainsArrayTypeError>);

class ComparisonExpressionContainsArrayTypeError final : public std::exception {
  public:
    const AST::ComparisonExpression& comparison_expression;

  public:
    explicit ComparisonExpressionContainsArrayTypeError(
        const AST::ComparisonExpression& comparison_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<ComparisonExpressionContainsArrayTypeError>);

class BooleanAndExpressionContainsArrayTypeError final : public std::exception {
  public:
    const AST::BooleanAndExpression& boolean_and_expression;

  public:
    explicit BooleanAndExpressionContainsArrayTypeError(
        const AST::BooleanAndExpression& boolean_and_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<BooleanAndExpressionContainsArrayTypeError>);

class BooleanOrExpressionContainsArrayTypeError final : public std::exception {
  public:
    const AST::BooleanOrExpression& boolean_or_expression;

  public:
    explicit BooleanOrExpressionContainsArrayTypeError(const AST::BooleanOrExpression& boolean_or_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<BooleanOrExpressionContainsArrayTypeError>);

class BooleanPartOfComparisonExpressionError final : public std::exception {
  public:
    const AST::ComparisonExpression& comparison_expression;

  public:
    explicit BooleanPartOfComparisonExpressionError(const AST::ComparisonExpression& comparison_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<BooleanPartOfComparisonExpressionError>);

class NonBooleanPartOfBooleanAndExpressionError final : public std::exception {
  public:
    const AST::BooleanAndExpression& boolean_and_expression;

  public:
    explicit NonBooleanPartOfBooleanAndExpressionError(
        const AST::BooleanAndExpression& boolean_and_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonBooleanPartOfBooleanAndExpressionError>);

class NonBooleanPartOfBooleanOrExpressionError final : public std::exception {
  public:
    const AST::BooleanOrExpression& boolean_or_expression;

  public:
    explicit NonBooleanPartOfBooleanOrExpressionError(const AST::BooleanOrExpression& boolean_or_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonBooleanPartOfBooleanOrExpressionError>);

class NonMatchingTypesInAssignmentStatementError final : public std::exception {
  public:
    const AST::Assignment& assignment;

  public:
    explicit NonMatchingTypesInAssignmentStatementError(const AST::Assignment& assignment) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonMatchingTypesInAssignmentStatementError>);

class NonBooleanExpressionInIfConditionError final : public std::exception {
  public:
    const AST::IfStatement& if_statement;

  public:
    explicit NonBooleanExpressionInIfConditionError(const AST::IfStatement& if_statement) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonBooleanExpressionInIfConditionError>);

class ArrayExpressionInIfConditionError final : public std::exception {
  public:
    const AST::IfStatement& if_statement;

  public:
    explicit ArrayExpressionInIfConditionError(const AST::IfStatement& if_statement) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<ArrayExpressionInIfConditionError>);

class NonBooleanPrimaryExpressionInPrefixNotExpression final : public std::exception {
  public:
    const AST::PrefixExpression& prefix_expression;

  public:
    explicit NonBooleanPrimaryExpressionInPrefixNotExpression(const AST::PrefixExpression& prefix_expression) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonBooleanPrimaryExpressionInPrefixNotExpression>);

class ArrayTypeAsArrayValueError final : public std::exception {
  public:
    const AST::ArrayLiteral& array_literal;

  public:
    explicit ArrayTypeAsArrayValueError(const AST::ArrayLiteral& array_literal) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<ArrayTypeAsArrayValueError>);

class ArrayTypeAsArraySizeError final : public std::exception {
  public:
    const AST::ArrayLiteral& array_literal;

  public:
    explicit ArrayTypeAsArraySizeError(const AST::ArrayLiteral& array_literal) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<ArrayTypeAsArraySizeError>);

class NonIntegerTypeAsArraySizeError final : public std::exception {
  public:
    const AST::ArrayLiteral& array_literal;

  public:
    explicit NonIntegerTypeAsArraySizeError(const AST::ArrayLiteral& array_literal) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonIntegerTypeAsArraySizeError>);

class SubscriptOnNonArrayVariableError final : public std::exception {
  public:
    const AST::VariableReference& variable_reference;

  public:
    explicit SubscriptOnNonArrayVariableError(const AST::VariableReference& variable_reference) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<SubscriptOnNonArrayVariableError>);

class NonIntegerSubscriptError final : public std::exception {
  public:
    const AST::VariableReference& variable_reference;

  public:
    explicit NonIntegerSubscriptError(const AST::VariableReference& variable_reference) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<NonIntegerSubscriptError>);

class LenFunctionCallOnNonArrayError final : public std::exception {
  public:
    const AST::LenFunctionCall& len_function_call;

  public:
    explicit LenFunctionCallOnNonArrayError(const AST::LenFunctionCall& len_function_call) noexcept;

    [[nodiscard]] const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<LenFunctionCallOnNonArrayError>);

}

#endif
