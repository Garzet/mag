#ifndef MAL_PRINTVISITOR_HPP
#define MAL_PRINTVISITOR_HPP

#include "MAL/AbstractSyntaxTree.hpp"

#include <ostream>

namespace MAL::AST {

class PrintVisitor final {
  private:
    class Indenter final {
      private:
        unsigned indent;

      public:
        explicit Indenter(unsigned indent) noexcept;

        [[nodiscard]] Indenter operator+(int increase) const noexcept;
        friend std::ostream& operator<<(std::ostream& os, const Indenter& in) noexcept;
    };

  private:
    std::ostream& os;
    Indenter indent;

  public:
    explicit PrintVisitor(std::ostream& os, unsigned indent = 0) noexcept;

    void operator()(PrefixOperator op) const noexcept;
    void operator()(AdditionOperator op) const noexcept;
    void operator()(MultiplicationOperator op) const noexcept;
    void operator()(ComparisonOperator op) const noexcept;
    void operator()(LogicalOperator op) const noexcept;
    void operator()(AssignmentOperator op) const noexcept;

    void operator()(MainKeyword main_keyword) const noexcept;
    void operator()(TypeKeyword type_keyword) const noexcept;

    void operator()(const VariableIdentifier& var_id) const noexcept;
    void operator()(const VariableReference& var_ref) const noexcept;
    void operator()(const NumericLiteral& nl) const noexcept;
    void operator()(const ArrayLiteral& al) const noexcept;
    void operator()(const LenFunctionCall& lfc) const noexcept;
    void operator()(const VariableDeclaration& decl) const noexcept;
    void operator()(const StringLiteral& sl) const noexcept;
    void operator()(const PrimaryExpression& pe) const noexcept;
    void operator()(const PrefixExpression& pe) const noexcept;
    void operator()(const MultiplicationExpression& me) const noexcept;
    void operator()(const AdditionExpression& ae) const noexcept;
    void operator()(const ComparisonExpression& ce) const noexcept;
    void operator()(const BooleanAndExpression& bae) const noexcept;
    void operator()(const BooleanOrExpression& boe) const noexcept;

    void operator()(const Assignment& a) const noexcept;
    void operator()(const Send& s) const noexcept;
    void operator()(const Receive& r) const noexcept;
    void operator()(const Print& p) const noexcept;
    void operator()(const IfStatement& is) const noexcept;
    void operator()(const ForStatement& fs) const noexcept;

    void apply_to_all(const std::vector<Statement>& statements) const noexcept;

  private:
    PrintVisitor(std::ostream& os, Indenter indenter) noexcept;

    friend std::ostream& operator<<(std::ostream& os, const Indenter& i) noexcept;
};

std::ostream& operator<<(std::ostream& os, PrefixOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, AdditionOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, MultiplicationOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, ComparisonOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, LogicalOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, AssignmentOperator op) noexcept;

std::ostream& operator<<(std::ostream& os, MainKeyword main_keyword) noexcept;

std::ostream& operator<<(std::ostream& os, TypeKeyword type_keyword) noexcept;

}

#endif
