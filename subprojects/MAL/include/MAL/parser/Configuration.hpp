#ifndef MAL_PARSER_CONFIGURATION_HPP
#define MAL_PARSER_CONFIGURATION_HPP

#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/home/x3/support/utility/error_reporting.hpp>

#include <string_view>

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

using Iterator = std::string_view::const_iterator;

// Tag used to inject and retrieve errror handler into and from the parsers.
struct ErrorHandlerTag;

using ErrorHandler  = boost::spirit::x3::error_handler<Iterator>;
using PhraseContext = x3::phrase_parse_context<x3::space_type>::type;

using Context = x3::context<ErrorHandlerTag, std::reference_wrapper<ErrorHandler>, PhraseContext>;

}

#endif
