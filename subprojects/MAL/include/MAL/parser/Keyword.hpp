#ifndef MAL_PARSER_KEYWORD_HPP
#define MAL_PARSER_KEYWORD_HPP

#include <boost/spirit/home/x3.hpp>
#include "MAL/parser/Annotators.hpp"

#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

struct KeywordSendRID final : AnnotateOnSuccess {};
using KeywordSendRule = x3::rule<KeywordSendRID, AST::MainKeyword>;

struct KeywordReceiveRID final : AnnotateOnSuccess {};
using KeywordReceiveRule = x3::rule<KeywordReceiveRID, AST::MainKeyword>;

struct KeywordPrintRID final : AnnotateOnSuccess {};
using KeywordPrintRule = x3::rule<KeywordPrintRID, AST::MainKeyword>;

struct KeywordIfRID final : AnnotateOnSuccess {};
using KeywordIfRule = x3::rule<KeywordIfRID, AST::MainKeyword>;

struct KeywordForRID final : AnnotateOnSuccess {};
using KeywordForRule = x3::rule<KeywordForRID, AST::MainKeyword>;

struct KeywordElseRID final : AnnotateOnSuccess {};
using KeywordElseRule = x3::rule<KeywordElseRID, AST::MainKeyword>;

struct KeywordVarRID final : AnnotateOnSuccess {};
using KeywordVarRule = x3::rule<KeywordVarRID, AST::MainKeyword>;

struct MainKeywordRID final : AnnotateOnSuccess {};
using MainKeywordRule = x3::rule<MainKeywordRID, AST::MainKeyword>;

struct TypeKeywordRID final : AnnotateOnSuccess {};
using TypeKeywordRule = x3::rule<TypeKeywordRID, AST::TypeKeyword>;

struct BooleanLiteralKeywordRID final : AnnotateOnSuccess {};
using BooleanLiteralKeywordRule = x3::rule<BooleanLiteralKeywordRID, AST::BooleanLiteralKeyword>;

struct KeywordRID final : AnnotateOnSuccess {};
using KeywordRule = x3::rule<KeywordRID, AST::Keyword>;

BOOST_SPIRIT_DECLARE(KeywordSendRule,
                     KeywordReceiveRule,
                     KeywordPrintRule,
                     KeywordIfRule,
                     KeywordForRule,
                     KeywordElseRule,
                     KeywordVarRule,
                     MainKeywordRule,
                     TypeKeywordRule,
                     KeywordRule,
                     BooleanLiteralKeywordRule,
                     KeywordRule)

}

namespace MAL {

[[nodiscard]] const Parser::KeywordSendRule& get_keyword_send_rule() noexcept;

[[nodiscard]] const Parser::KeywordReceiveRule& get_keyword_receive_rule() noexcept;

[[nodiscard]] const Parser::KeywordPrintRule& get_keyword_print_rule() noexcept;

[[nodiscard]] const Parser::KeywordIfRule& get_keyword_if_rule() noexcept;

[[nodiscard]] const Parser::KeywordForRule& get_keyword_for_rule() noexcept;

[[nodiscard]] const Parser::KeywordElseRule& get_keyword_else_rule() noexcept;

[[nodiscard]] const Parser::KeywordVarRule& get_keyword_var_rule() noexcept;

[[nodiscard]] const Parser::MainKeywordRule& get_main_keyword_rule() noexcept;

[[nodiscard]] const Parser::TypeKeywordRule& get_type_keyword_rule() noexcept;

[[nodiscard]] const Parser::BooleanLiteralKeywordRule& get_boolean_literal_keyword_rule() noexcept;

[[nodiscard]] const Parser::KeywordRule& get_keyword_rule() noexcept;

}

#endif
