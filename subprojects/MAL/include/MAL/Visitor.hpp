#ifndef MAL_VISITOR_HPP
#define MAL_VISITOR_HPP

namespace MAL {

template<typename... Base>
struct Visitor final : Base... {
    using Base::operator()...;
};

template<typename... T>
Visitor(T...) -> Visitor<T...>;

}

#endif
