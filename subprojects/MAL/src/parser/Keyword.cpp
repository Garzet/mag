#include "MAL/parser/Keyword.hpp"

#include "MAL/AbstractSyntaxTreeAdapted.hpp"

namespace MAL::Parser {

using x3::lit;
using x3::alnum;
using x3::char_;

struct TypeKeywordSymbolTable : x3::symbols<AST::TypeKeyword> {
    TypeKeywordSymbolTable() noexcept
    {
        // clang-format off
        add("float", AST::TypeKeyword::Float)
           ("int",   AST::TypeKeyword::Int)
           ("bool",  AST::TypeKeyword::Bool);
        // clang-format on
    }
};
const TypeKeywordSymbolTable type_keyword_symbol_table;

struct BooleanLiteralKeywordSymbolTable : x3::symbols<AST::BooleanLiteralKeyword> {
    BooleanLiteralKeywordSymbolTable() noexcept
    {
        // clang-format off
        add("true",  AST::BooleanLiteralKeyword::True)
           ("false", AST::BooleanLiteralKeyword::False);
        // clang-format on
    }
};
const BooleanLiteralKeywordSymbolTable boolean_literal_keyword_symbol_table;

struct MainKeywordSymbolTable : x3::symbols<AST::MainKeyword> {
    MainKeywordSymbolTable() noexcept
    {
        // clang-format off
        add("send",    AST::MainKeyword::Send)
           ("receive", AST::MainKeyword::Receive)
           ("print",   AST::MainKeyword::Print)
           ("if",      AST::MainKeyword::If)
           ("for",     AST::MainKeyword::For)
           ("else",    AST::MainKeyword::Else)
           ("var",     AST::MainKeyword::Var);
        // clang-format on
    }
};
const MainKeywordSymbolTable main_keyword_symbol_table;

// Helper to avoid keyword parser defined as a symbol table be greedy (avoid
// matching substrings, e.g. do not match "int" in "integer" and have "eger"
// leftover, the desired behavior is to not match anything and have "integer"
// leftover).
const auto nongreedy = [](auto parser) { return x3::lexeme[parser >> !(alnum | x3::char_('_'))]; };

const MainKeywordRule main_keyword = "main_keyword";
const auto main_keyword_def        = nongreedy(main_keyword_symbol_table);

const TypeKeywordRule type_keyword = "type_keyword";
const auto type_keyword_def        = nongreedy(type_keyword_symbol_table);

const BooleanLiteralKeywordRule boolean_literal_keyword = "boolean_literal_keyword";
const auto boolean_literal_keyword_def                  = nongreedy(boolean_literal_keyword_symbol_table);

const KeywordRule keyword = "keyword";
const auto keyword_def    = main_keyword | type_keyword | boolean_literal_keyword;

const KeywordSendRule keyword_send = "keyword_send";
const auto keyword_send_def        = &lit("send") >> main_keyword;

const KeywordReceiveRule keyword_receive = "keyword_receive";
const auto keyword_receive_def           = &lit("receive") >> main_keyword;

const KeywordPrintRule keyword_print = "keyword_print";
const auto keyword_print_def         = &lit("print") >> main_keyword;

const KeywordIfRule keyword_if = "keyword_if";
const auto keyword_if_def      = &lit("if") >> main_keyword;

const KeywordForRule keyword_for = "keyword_for";
const auto keyword_for_def       = &lit("for") >> main_keyword;

const KeywordElseRule keyword_else = "keyword_else";
const auto keyword_else_def        = &lit("else") >> main_keyword;

const KeywordVarRule keyword_var = "keyword_var";
const auto keyword_var_def       = &lit("var") >> main_keyword;

BOOST_SPIRIT_DEFINE(keyword_send,
                    keyword_receive,
                    keyword_print,
                    keyword_if,
                    keyword_for,
                    keyword_else,
                    keyword_var,
                    main_keyword,
                    type_keyword,
                    boolean_literal_keyword,
                    keyword)

}

namespace MAL {

const Parser::KeywordSendRule& get_keyword_send_rule() noexcept
{
    return Parser::keyword_send;
}

const Parser::KeywordReceiveRule& get_keyword_receive_rule() noexcept
{
    return Parser::keyword_receive;
}

const Parser::KeywordPrintRule& get_keyword_print_rule() noexcept
{
    return Parser::keyword_print;
}

const Parser::KeywordIfRule& get_keyword_if_rule() noexcept
{
    return Parser::keyword_if;
}

const Parser::KeywordForRule& get_keyword_for_rule() noexcept
{
    return Parser::keyword_for;
}

const Parser::KeywordElseRule& get_keyword_else_rule() noexcept
{
    return Parser::keyword_else;
}

const Parser::KeywordVarRule& get_keyword_var_rule() noexcept
{
    return Parser::keyword_var;
}

const Parser::MainKeywordRule& get_main_keyword_rule() noexcept
{
    return Parser::main_keyword;
}

const Parser::TypeKeywordRule& get_type_keyword_rule() noexcept
{
    return Parser::type_keyword;
}

const Parser::BooleanLiteralKeywordRule& get_boolean_literal_keyword_rule() noexcept
{
    return Parser::boolean_literal_keyword;
}

const Parser::KeywordRule& get_keyword_rule() noexcept
{
    return Parser::keyword;
}

}

#include "MAL/parser/Configuration.hpp"
namespace MAL::Parser {
BOOST_SPIRIT_INSTANTIATE(KeywordSendRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordReceiveRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordPrintRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordIfRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordForRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordElseRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordVarRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(MainKeywordRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(TypeKeywordRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(BooleanLiteralKeywordRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(KeywordRule, Iterator, Context)
}
