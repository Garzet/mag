#include "MAL/parser/Operator.hpp"

#include "MAL/AbstractSyntaxTreeAdapted.hpp"

namespace MAL::Parser {

struct PrefixOperatorSymbolTable : x3::symbols<AST::PrefixOperator> {
    PrefixOperatorSymbolTable()
    {
        // clang-format off
        add("!", AST::PrefixOperator::Not);
        // clang-format on
    }
};
const PrefixOperatorSymbolTable prefix_operator_symbol_table;

struct MultiplicationOperatorSymbolTable : x3::symbols<AST::MultiplicationOperator> {
    MultiplicationOperatorSymbolTable()
    {
        // clang-format off
        add("*", AST::MultiplicationOperator::Multiply)
           ("/", AST::MultiplicationOperator::Divide);
        // clang-format on
    }
};
const MultiplicationOperatorSymbolTable multiplication_operator_symbol_table;

struct AdditionOperatorSymbolTable : x3::symbols<AST::AdditionOperator> {
    AdditionOperatorSymbolTable()
    {
        // clang-format off
        add("+", AST::AdditionOperator::Plus)
           ("-", AST::AdditionOperator::Minus);
        // clang-format on
    }
};
const AdditionOperatorSymbolTable addition_operator_symbol_table;

struct ComparisonOperatorSymbolTable : x3::symbols<AST::ComparisonOperator> {
    ComparisonOperatorSymbolTable()
    {
        // clang-format off
        add("==", AST::ComparisonOperator::Equal)
           ("!=", AST::ComparisonOperator::NotEqual)
           ( "<", AST::ComparisonOperator::Less)
           ( ">", AST::ComparisonOperator::Greater)
           ("<=", AST::ComparisonOperator::LessOrEqual)
           (">=", AST::ComparisonOperator::GreaterOrEqual);
        // clang-format on
    }
};
const ComparisonOperatorSymbolTable comparison_operator_symbol_table;

struct LogicalOperatorSymbolTable : x3::symbols<AST::LogicalOperator> {
    LogicalOperatorSymbolTable()
    {
        // clang-format off
        add("&&", AST::LogicalOperator::And)
           ("||", AST::LogicalOperator::Or);
        // clang-format on
    }
};
const LogicalOperatorSymbolTable logical_operator_symbol_table;

struct AssignmentOperatorSymbolTable : x3::symbols<AST::AssignmentOperator> {
    AssignmentOperatorSymbolTable() { add("=", AST::AssignmentOperator::Equals); }
};
const AssignmentOperatorSymbolTable assignment_operator_symbol_table;

const PrefixOperatorRule prefix_operator{"prefix_oprator"};
const auto prefix_operator_def = prefix_operator_symbol_table;

const MultiplicationOperatorRule multiplication_operator{"multiplication_oprator"};
const auto multiplication_operator_def = multiplication_operator_symbol_table;

const AdditionOperatorRule addition_operator{"addition_operator"};
const auto addition_operator_def = addition_operator_symbol_table;

const ComparisonOperatorRule comparison_operator{"comparison_operator"};
const auto comparison_operator_def = comparison_operator_symbol_table;

const LogicalOperatorRule logical_operator{"logical_operator"};
const auto logical_operator_def = logical_operator_symbol_table;

const AssignmentOperatorRule assignment_operator{"assignment_oprator"};
const auto assignment_operator_def = assignment_operator_symbol_table;

BOOST_SPIRIT_DEFINE(prefix_operator,
                    multiplication_operator,
                    addition_operator,
                    comparison_operator,
                    logical_operator,
                    assignment_operator)

}

namespace MAL {

const Parser::PrefixOperatorRule& get_prefix_operator_rule() noexcept
{
    return Parser::prefix_operator;
}

const Parser::MultiplicationOperatorRule& get_multiplication_operator_rule() noexcept
{
    return Parser::multiplication_operator;
}

const Parser::AdditionOperatorRule& get_addition_operator_rule() noexcept
{
    return Parser::addition_operator;
}

const Parser::ComparisonOperatorRule& get_comparison_operator_rule() noexcept
{
    return Parser::comparison_operator;
}

const Parser::LogicalOperatorRule& get_logical_operator_rule() noexcept
{
    return Parser::logical_operator;
}

const Parser::AssignmentOperatorRule& get_assignment_operator_rule() noexcept
{
    return Parser::assignment_operator;
}

}

#include "MAL/parser/Configuration.hpp"
namespace MAL::Parser {
BOOST_SPIRIT_INSTANTIATE(PrefixOperatorRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(MultiplicationOperatorRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(AdditionOperatorRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(ComparisonOperatorRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(LogicalOperatorRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(AssignmentOperatorRule, Iterator, Context)
}
