#include <boost/test/unit_test.hpp>

#include "application/BinaryOperationHandler.hpp"

BOOST_AUTO_TEST_SUITE(application);

BOOST_AUTO_TEST_SUITE(binary_operation_handler);

BOOST_AUTO_TEST_CASE(multicloud_arithmetic_operation_results_in_distributed_code)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 200}, MAO::Host{"H2", 300}, MAO::Host{"H3", 400}};

    MAG::Out::Expression lhs{MAG::Out::VariableReference{std::nullopt, "a", std::nullopt}};
    MAG::Out::Expression rhs{MAG::Out::VariableReference{std::nullopt, "b", std::nullopt}};

    const auto [asts, partial_expressions] =
        MAG::Application::handle_multicloud_operation(hosts[0],
                                                      std::move(lhs),
                                                      hosts[1],
                                                      std::move(rhs),
                                                      {hosts[2], hosts[3]},
                                                      MAL::AST::AdditionOperator::Plus,
                                                      MAL::AST::TypeKeyword::Int);

    BOOST_REQUIRE_EQUAL(asts.size(), 2);
    {
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 1);
        const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[0]);
        BOOST_REQUIRE(fn_call);
        BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_initial<Add, int>");
        BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 3);
        {
            const auto* const var_decl = boost::get<const MAG::Out::VariableReference>(&fn_call->arguments[0]);
            BOOST_REQUIRE(var_decl);
            BOOST_CHECK_EQUAL(var_decl->variable_name, "a");
        }
        {
            const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[1]);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
        }
        {
            const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[2]);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
        }
    }
    {
        const auto it = asts.find(hosts[1]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 1);
        const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[0]);
        BOOST_REQUIRE(fn_call);
        BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_intermediate<Add, int>");
        BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 3);
        {
            const auto* const var_decl = boost::get<const MAG::Out::VariableReference>(&fn_call->arguments[0]);
            BOOST_REQUIRE(var_decl);
            BOOST_CHECK_EQUAL(var_decl->variable_name, "b");
        }
        {
            const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[1]);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
        }
        {
            const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[2]);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
        }
    }

    BOOST_REQUIRE_EQUAL(partial_expressions.size(), 2);
    {
        const auto it = partial_expressions.find(hosts[2]);
        BOOST_REQUIRE(it != partial_expressions.cend());
        const auto* const write_and_return_fn_call = boost::get<MAG::Out::FunctionCall>(&it->second);
        BOOST_REQUIRE(write_and_return_fn_call);
        BOOST_CHECK_EQUAL(write_and_return_fn_call->function_name, "write_and_return");
        BOOST_REQUIRE_EQUAL(write_and_return_fn_call->arguments.size(), 2);
        {
            const auto* const fn_call = boost::get<MAG::Out::FunctionCall>(&write_and_return_fn_call->arguments[0]);
            BOOST_REQUIRE(fn_call);
            BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_final<Add, int>");
            BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
            {
                const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
            }
            {
                const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&fn_call->arguments[1]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
            }
        }
        {
            const auto* const il = boost::get<MAG::Out::InitializerList>(&write_and_return_fn_call->arguments[1]);
            BOOST_REQUIRE(il);
            BOOST_REQUIRE_EQUAL(il->expressions.size(), 1);
            {
                const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&il->expressions[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[3].get_name());
            }
        }
    }
    {
        const auto it = partial_expressions.find(hosts[3]);
        BOOST_REQUIRE(it != partial_expressions.cend());
        const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&it->second);
        BOOST_REQUIRE(read_fn_call);
        BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
        BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
        {
            const auto* const lit = boost::get<const MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
        }
    }
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
